﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;

public enum GameBGM
{
    Title,
    Stage,
    Boss,
}

public enum GameSE
{
    MenuCursor,
    CursorEnter,
    CursorCancel,
    PlayerWalk,
    PlayerJump,
    PlayerLanding,
    PlayerDamage,
    PlayerShot,
    EnemyDead,
    EnemyShot,
    Fanfale,
    Explosion,
    ItemGet,
}

public static class BgmNames
{
    public static Dictionary<GameBGM, string> GetBgmDictionary()
    {
        return new Dictionary<GameBGM, string>()
        {
            { GameBGM.Title,"bgm_title" },
            { GameBGM.Stage, "bgm_map" },
            { GameBGM.Boss, "bgm_boss" },
        };
    }
}

public static class SeNames
{
    public static Dictionary<GameSE, string> GetSeDictionary()
    {
        return new Dictionary<GameSE, string>()
        {
            { GameSE.MenuCursor, "se_menucursor" },          // メニュー：カーソル
            { GameSE.CursorEnter, "se_cursorenter" },        // メニュー：決定
            { GameSE.CursorCancel, "se_cursorcancel" },      // メニュー：キャンセル

            { GameSE.PlayerWalk, "se_walk" },
            { GameSE.PlayerJump, "se_suzujump" },
            { GameSE.PlayerLanding, "se_landing" },
            { GameSE.PlayerDamage, "se_suzudamage2" },
            { GameSE.PlayerShot, "se_hitode" },
            { GameSE.EnemyDead, "se_enemydead" },
            { GameSE.EnemyShot, "se_magic" },
            { GameSE.Fanfale, "se_goal" },
            { GameSE.Explosion, "se_explosion" },
            { GameSE.ItemGet, "se_hyoi2" },
        };
    }
}


public class AudioManager : SingletonMonoBehaviour<AudioManager>
{
    private static readonly string BaseResourceDir = "Sounds";
    private static readonly string BgmDirName = "Bgm";
    private static readonly string SeDirName = "Se";

    [SerializeField]
    [Range(0, 1)]
    private static readonly float BgmVolume = 0.1f;

    [SerializeField]
    [Range(0, 1)]
    private static readonly float SeVolume = 0.12f;

    private static readonly int SeChanels = 4;

    private enum AudioType
    {
        Bgm,
        Se,
    }

    private GameObject audioSourcesObj = null;

    private AudioSource sourceBgm = null;
    private AudioSource sourceSeDefault = null;
    private AudioSource[] audioSources;

    private Dictionary<GameBGM, AudioClip> bgmPool = new Dictionary<GameBGM, AudioClip>();
    private Dictionary<GameSE, AudioClip> sePool = new Dictionary<GameSE, AudioClip>();

    private class AudioData
    {
        public string Key;
        public AudioClip AudioClip;

        public AudioData(string key, AudioClip audioClip)
        {
            Key = key;
            AudioClip = audioClip;
        }
    }

    public AudioManager()
    {
        audioSources = new AudioSource[SeChanels];
    }

    protected override void Awake()
    {
        base.Awake();
        foreach (var item in BgmNames.GetBgmDictionary())
        {
            LoadBgm(item.Key, item.Value);
        }
        foreach (var item in SeNames.GetSeDictionary())
        {
            LoadSe(item.Key, item.Value);
        }
    }

    private void Start()
    {
        SetSeVolume(SeVolume);
        for (int i = 0; i < SeChanels; i++)
        {
            SetSeVolume(SeVolume, i);
        }
    }

    private AudioSource GetAudioSource(AudioType type, int channel = -1)
    {
        if (audioSourcesObj == null)
        {
            // GameObjectがなければ作る
            audioSourcesObj = new GameObject("AudioSources");
            audioSourcesObj.transform.SetParent(transform);

            // 破棄しないようにする
            //DontDestroyOnLoad(audioSourcesObj);

            // AudioSourceを作成
            sourceBgm = audioSourcesObj.AddComponent<AudioSource>();
            sourceSeDefault = audioSourcesObj.AddComponent<AudioSource>();
            for (int i = 0; i < SeChanels; i++)
            {
                audioSources[i] = audioSourcesObj.AddComponent<AudioSource>();
            }
        }

        if (type == AudioType.Bgm)
        {
            // BGM
            return sourceBgm;
        }
        else
        {
            // SE
            if (0 <= channel && channel < SeChanels)
            {
                // チャンネル指定
                return audioSources[channel];
            }
            else
            {
                // デフォルト
                return sourceSeDefault;
            }
        }
    }


    public static void LoadBgm(GameBGM key, string resName)
    {
        Instance.LoadBgmCore(key, resName);
    }
    public static void LoadBgm(GameBGM key, AudioClip bgmClip)
    {
        Instance.LoadBgmCore(key, bgmClip);
    }
    public static void LoadSe(GameSE key, string resName)
    {
        Instance.LoadSeCore(key, resName);
    }
    private void LoadBgmCore(GameBGM key, string resName)
    {
        string path = Path.Combine(BaseResourceDir, BgmDirName);
        path = Path.Combine(path, resName);
        LoadBgmCore(key, Resources.Load(path) as AudioClip);
    }

    private void LoadBgmCore(GameBGM key, AudioClip bgmClip)
    {
        // すでに登録済みだった場合、上書きする
        if (bgmPool.ContainsKey(key))
        {
            bgmPool.Remove(key);
        }
        bgmPool.Add(key, bgmClip);
    }
    public static void UnloadBgm(GameBGM key)
    {
        Instance.UnloadBgmCore(key);
    }
    private void UnloadBgmCore(GameBGM key)
    {
        bgmPool.Remove(key);
    }
    private void LoadSeCore(GameSE key, string resName)
    {
        // すでに登録済みだった場合、上書きする
        if (sePool.ContainsKey(key))
        {
            sePool.Remove(key);
        }
        string path = Path.Combine(BaseResourceDir, SeDirName);
        path = Path.Combine(path, resName);
        sePool.Add(key, Resources.Load(path) as AudioClip);
    }


    public static bool PlayBgm(GameBGM key)
    {
        return Instance.PlayBgmCore(key);
    }
    private bool PlayBgmCore(GameBGM key)
    {
        if (bgmPool.ContainsKey(key) == false)
        {
            Debug.LogWarning("PlayBgm key=" + key + "は読み込まれていません！");
            return false;
        }

        // いったん止める
        StopBgmCore();

        // リソースの取得
        var _data = bgmPool[key];

        // 再生
        var source = GetAudioSource(AudioType.Bgm);
        source.loop = true;
        source.clip = _data;
        source.volume = BgmVolume;
        source.Play();

        return true;
    }
    public static bool StopBgm()
    {
        return Instance.StopBgmCore();
    }
    private bool StopBgmCore()
    {
        GetAudioSource(AudioType.Bgm).Stop();
        return true;
    }

    /// <summary>
    /// 効果音を鳴らす処理
    /// </summary>
    /// <param name="key">どの効果音を鳴らすか</param>
    /// <param name="demoPlaySE">trueの場合デモ中も効果音を鳴らす</param>
    /// <param name="channel">出力チャネル(通常は-1のままでOK)</param>
    /// <returns></returns>
    public static bool PlaySe(GameSE key, bool demoPlaySE = false, int channel = -1)
    {
        if (AppManager.Instance.CurrentSceneState == SceneState.Game)
        {
            if (!demoPlaySE && InGameManager.Instance.IsEventPlaying.Value)
            {
                return false;
            }
        }

        return Instance.PlaySeCore(key, channel);
    }
    private bool PlaySeCore(GameSE key, int channel = -1)
    {
        if (sePool.ContainsKey(key) == false)
        {
            Debug.LogWarning("PlaySe key=" + key + "は読み込まれていません！");
            return false;
        }

        // リソースの取得
        var _data = sePool[key];

        if (0 <= channel && channel < SeChanels)
        {
            // チャンネル指定
            var source = GetAudioSource(AudioType.Se, channel);
            source.clip = _data;
            source.Play();
        }
        else
        {
            // デフォルトで再生
            var source = GetAudioSource(AudioType.Se);
            source.PlayOneShot(_data);
        }

        return true;
    }
    public static void SetSeVolume(float volume, int channel = -1)
    {
        Instance.SetSeVolumeCore(volume, channel);
    }
    private void SetSeVolumeCore(float volume, int channel = -1)
    {
        if (0 <= channel && channel < SeChanels)
        {
            var source = GetAudioSource(AudioType.Se, channel);
            source.volume = volume;
        }
        else
        {
            var source = GetAudioSource(AudioType.Se);
            source.volume = volume;
        }
    }
}
