﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
//using MsgPack;
using System.IO;
using System.Security.Cryptography;
using UnityConstants;
using UniRx;
using UnityEngine.UI;
using DG.Tweening;

[Serializable]
public enum StageIndex
{
    Stage001,
    Ending,
}

[Serializable]
public enum WorldIndex
{
    World1,
}

[Serializable]
public enum DemoIndex
{
    OpDemo,
}

[Serializable]
class StageProgress
{
    // クリアしているか
    public bool cleared = false;

    // メダル(実績)
    // 0: 規定タイム以内でクリア
    // 1: 体力満タンでクリア
    // 2: メダルをゲットしてクリア
    public bool[] medal = {false, false, false};

    public float clearTime;
}

// NOTE : セーブデータに何を保存するかまだ未定だが、
// とりあえずクリアしたステージをBitArrayで保存することに。
[Serializable]
class SaveData
{
    // セーブデータバージョン指定（デバッグ用）
    public int DataVersion { get; set; }

    // クリア済みステージ
    public Dictionary<StageIndex, StageProgress> Stage { get; set; }

    // 見終わっているデモ
    public Dictionary<DemoIndex, bool> Demo { get; set; }
}

public class ConfigData
{
    public int BgmVolume { get; set; }
    public int SeVolume { get; set; }
    public bool isJapanese { get; set; }

    // 0: 憑依
    // 1: 憑依解放
    // 2: ジャンプ
    // 3: ポーズ
    public Dictionary<GameKey, KeyCode> padConfig { get; set; }
}

public class SaveDataManager : SingletonMonoBehaviour<SaveDataManager>
{
    private static readonly string EncryptKey = "og6ajna9vnae3ui1vu0hf2joai4e3gea";
    private static readonly int EncryptPasswordCount = 16;
    private static readonly string PasswordChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static readonly int PasswordCharsLength = PasswordChars.Length;

    //private static readonly ObjectPacker Packer = new ObjectPacker();

    // デバッグ用
    public int SaveDataVersion { get; } = 6;

    // セーブデータ本体
    private SaveData currentSaveData = new SaveData();

    // セーブデータ読み込み成功フラグ
    public bool IsLoadSucceed { get; private set; }

    // コンフィグデータ本体
    private ConfigData currentConfigData = new ConfigData();

    // コンフィグデータ読み込み成功フラグフラグ
    public bool IsConfigLoadSucceed { get; private set; }

    /// <summary>
    /// 新しいステージをクリアしたかのフラグ。NullならFalse、
    /// 値が入っている場合はそのステージをクリアしたことを表す
    /// </summary>
    public StageIndex? NewStageClear { get; set; } = null;

    // プレイ中のステージのインデックス情報
    public StageIndex PlayingStageIndex;

    public void Start()
    {
        Debug.Log(AppManager.Instance.SavePath);
    }

    public void SaveDataInitialize()
    {
        currentSaveData = new SaveData();
        currentSaveData.DataVersion = SaveDataVersion;
        var dic = new Dictionary<StageIndex, StageProgress>();
        int length = Enum.GetNames(typeof(StageIndex)).Length;
        for (int i = 0; i < length; i++)
        {
            dic.Add((StageIndex)i, new StageProgress());
        }
        currentSaveData.Stage = dic;

        var demo = new Dictionary<DemoIndex, bool>();
        length = Enum.GetNames(typeof(DemoIndex)).Length;
        for (int i = 0; i < length; i++)
        {
            demo.Add((DemoIndex)i, false);
        }
        currentSaveData.Demo = demo;

    }

    // セーブデータ読み込み時、最後にクリアしたステージ
    public StageIndex GetLastedStageIndex()
    {
        int length = 32;
        for (int i = 0; i < length; i++)
        {
            if (!currentSaveData.Stage[(StageIndex)i].cleared)
            {
                return (StageIndex)i;
            }
        }

        // 全ステージクリア済みなら最終ステージ
        return (StageIndex)(length - 1);
    }

    public KeyCode GetPadConfig(GameKey gameKey)
    {
        return currentConfigData.padConfig[gameKey];
    }

    public ConfigData GetConfigData()
    {
        return currentConfigData;
    }
    public bool SetConfigData(ConfigData conf)
    {
        // エラーチェック
        if (conf.BgmVolume < 0 || conf.BgmVolume > 5)
        {
            throw new Exception("BGM音量がおかしいなりぃ");
        }
        if (conf.SeVolume < 0 || conf.SeVolume > 5)
        {
            throw new Exception("SE音量がおかしいなりぃ");
        }
        foreach (var pad in conf.padConfig)
        {
            if (!pad.ToString().Contains("JoystickButton"))
            {
                throw new Exception("パッド設定がおかしいなりぃ");
            }
        }

        // 問題なければここにくる
        currentConfigData = conf;
        return true;
    }

    public void ConfigDataInitialize()
    {
        currentConfigData.BgmVolume = 5;
        currentConfigData.SeVolume = 5;
        currentConfigData.isJapanese = true;
        currentConfigData.padConfig = new Dictionary<GameKey, KeyCode>();
        currentConfigData.padConfig[GameKey.Action1] = KeyCode.JoystickButton1;
        currentConfigData.padConfig[GameKey.Action2] = KeyCode.JoystickButton2;
        currentConfigData.padConfig[GameKey.Camera] = KeyCode.JoystickButton3;
        currentConfigData.padConfig[GameKey.Jump] = KeyCode.JoystickButton0;
        currentConfigData.padConfig[GameKey.Pause] = KeyCode.JoystickButton7;
    }

    public void SaveGameProgress(StageIndex index, bool[] medal, float clearTime)
    {
        // 未クリアのステージをクリアしたとき
        if (!currentSaveData.Stage[index].cleared)
        {
            NewStageClear = index;
        }
        else
        {
            NewStageClear = null;
        }

        // 前回取得済みのメダルをfalseで上書かないようにする
        for (int i=0; i<currentSaveData.Stage[index].medal.Length; i++)
        {
            if (!currentSaveData.Stage[index].medal[i])
            {
                currentSaveData.Stage[index].medal[i] = medal[i];
            }
        }

        currentSaveData.Stage[index].cleared = true;
        currentSaveData.Stage[index].clearTime = clearTime;
    }

    public void SaveGameProgress(DemoIndex index)
    {
        currentSaveData.Demo[index] = true;
    }

    public bool GetGameProgress(StageIndex index)
    {
        return currentSaveData.Stage[index].cleared;
    }
    public bool[] GetMedalData(StageIndex index)
    {
        return currentSaveData.Stage[index].medal;
    }
    public float GetClearTimeData(StageIndex index)
    {
        return currentSaveData.Stage[index].clearTime;
    }
    public bool GetGameDemoProgress(DemoIndex index)
    {
        return currentSaveData.Demo[index];
    }

    void ShowDebug()
    {
        Debug.Log("---------");
        for (int i = 0; i < 5; i++)//currentSaveData.ClearedStage.Count; i++)
        {
            Debug.Log(Enum.GetName(typeof(StageIndex), i) + " : " + currentSaveData.Stage[(StageIndex)i]);
        }
        Debug.Log("---------");
    }

    public IObservable<Unit> TrySave()
    {
        Subject<Unit> subject = new Subject<Unit>();

        try
        {
            Debug.Log("[Save]DataVersion:" + currentSaveData.DataVersion);
            Save(currentSaveData, AppManager.Instance.SavePath);
            Debug.Log("[Save]Save Complete !!");

        }
        catch (Exception e)
        {
            Debug.LogError("[Save]Save Error !! : " + e);
            // セーブ失敗したときなんかゲーム側に警告出す？
        }

        return subject.AsObservable();
    }

    public void SaveConfig()
    {
        try
        {
            Save(currentConfigData, AppManager.Instance.ConfigDataPath);
        }
        catch (Exception e)
        {
            Debug.LogError("[Save]Config Save Error !! : " + e);
        }
    }

    public void TryLoad()
    {
        IsLoadSucceed = false;
        try
        {
            Debug.Log("[Load]Try Load ...");

            // 読み込み
            byte[] ivBytes = null;
            byte[] data = null;
            var savePath = AppManager.Instance.SavePath;
            using (FileStream fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    int length = br.ReadInt32();
                    ivBytes = br.ReadBytes(length);

                    length = br.ReadInt32();
                    data = br.ReadBytes(length);
                }
            }

            // 複合化
            string iv = Encoding.UTF8.GetString(ivBytes);
            byte[] pack;
            DecryptAes(data, iv, out pack);

            // セーブデータ復元
            //currentSaveData = Packer.Unpack<SaveData>(pack);

            Debug.Log("[Load]Load Successed !!");

            //ShowDebug();

            PlayingStageIndex = GetLastedStageIndex();
            IsLoadSucceed = true;

        }
        catch (FileNotFoundException)
        {
            Debug.Log("[Load]Save Dataがありません。データを初期化します");
            SaveDataInitialize();
        }
        catch (Exception e)
        {
            Debug.LogError("[Load]Load Error !! : " + e);
            // ロード失敗したときなんかゲーム側に警告出す？

            SaveDataInitialize();
        }
    }

    public void TryConfigLoad()
    {
        IsConfigLoadSucceed = false;
        try
        {
            Debug.Log("[Load]Try Config Data Load ...");

            // 読み込み
            byte[] ivBytes = null;
            byte[] data = null;
            var savePath = AppManager.Instance.ConfigDataPath;
            using (FileStream fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    int length = br.ReadInt32();
                    ivBytes = br.ReadBytes(length);

                    length = br.ReadInt32();
                    data = br.ReadBytes(length);
                }
            }

            // 複合化
            string iv = Encoding.UTF8.GetString(ivBytes);
            byte[] pack;
            DecryptAes(data, iv, out pack);

            // セーブデータ復元
            //currentConfigData = Packer.Unpack<ConfigData>(pack);

            Debug.Log("[Load]Load Config Data Successed !!");

            IsConfigLoadSucceed = true;
            //PlayingStageIndex = GetLastedStageIndex();

        }
        catch (FileNotFoundException)
        {
            Debug.Log("[Load]Config Dataがありません。データを初期化します");
            ConfigDataInitialize();
        }
        catch (Exception e)
        {
            Debug.LogError("[Load]Load Config Data Error !! : " + e);
            ConfigDataInitialize();
        }
    }


    public void TryDeleteSave()
    {
        try
        {
            var savePath = AppManager.Instance.SavePath;
            if (!File.Exists(savePath))
            {
                return;
            }
            File.Delete(savePath);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
        finally
        {
            SaveDataInitialize();
        }
    }


    private void Save(System.Object data, string path)
    {
        try
        {
            // 暗号化
            //byte[] pack = Packer.Pack(data);
            //string iv;
            //byte[] file;
            //EncryptAes(pack, out iv, out file);

            //// 保存
            //byte[] ivBytes = Encoding.UTF8.GetBytes(iv);
            //var savePath = path;
            //using (FileStream fs = new FileStream(savePath, FileMode.Create, FileAccess.Write))
            //using (BinaryWriter bw = new BinaryWriter(fs))
            //{
            //    bw.Write(ivBytes.Length);
            //    bw.Write(ivBytes);

            //    bw.Write(file.Length);
            //    bw.Write(file);
            //}
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    /// <summary>
    /// AES暗号化を実行
    /// </summary>
    private void EncryptAes(byte[] src, out string iv, out byte[] dst)
    {
        iv = CreatePassword(EncryptPasswordCount);
        dst = null;
        using (RijndaelManaged rijndael = new RijndaelManaged())
        {
            rijndael.Padding = PaddingMode.PKCS7;
            rijndael.Mode = CipherMode.CBC;
            rijndael.KeySize = 256;
            rijndael.BlockSize = 128;

            byte[] key = Encoding.UTF8.GetBytes(EncryptKey);
            byte[] vec = Encoding.UTF8.GetBytes(iv);

            using (ICryptoTransform encryptor = rijndael.CreateEncryptor(key, vec))
            using (MemoryStream ms = new MemoryStream())
            using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
            {
                cs.Write(src, 0, src.Length);
                cs.FlushFinalBlock();
                dst = ms.ToArray();
            }
        }
    }

    /// <summary>
    /// AES複合化を実行
    /// </summary>
    private void DecryptAes(byte[] src, string iv, out byte[] dst)
    {
        dst = new byte[src.Length];
        using (RijndaelManaged rijndael = new RijndaelManaged())
        {
            rijndael.Padding = PaddingMode.PKCS7;
            rijndael.Mode = CipherMode.CBC;
            rijndael.KeySize = 256;
            rijndael.BlockSize = 128;

            byte[] key = Encoding.UTF8.GetBytes(EncryptKey);
            byte[] vec = Encoding.UTF8.GetBytes(iv);

            using (ICryptoTransform decryptor = rijndael.CreateDecryptor(key, vec))
            using (MemoryStream ms = new MemoryStream(src))
            using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
            {
                cs.Read(dst, 0, dst.Length);
            }
        }
    }


    /// <summary>
    /// パスワード生成
    /// </summary>
    private string CreatePassword(int count)
    {
        StringBuilder sb = new StringBuilder(count);
        for (int i = count - 1; i >= 0; i--)
        {
            char c = PasswordChars[UnityEngine.Random.Range(0, PasswordCharsLength)];
            sb.Append(c);
        }
        return sb.ToString();
    }

}
