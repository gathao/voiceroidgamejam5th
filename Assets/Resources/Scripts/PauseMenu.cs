﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField]
    Text ResumeGameText = null;

    [SerializeField]
    GameObject ResumeGameTextBack = null;

    [SerializeField]
    Text RestartText = null;

    [SerializeField]
    GameObject RestartTextBack = null;

    [SerializeField]
    Text OptionsText = null;

    [SerializeField]
    GameObject OptionsTextBack = null;

    [SerializeField]
    Text QuitGameText = null;

    [SerializeField]
    GameObject QuitGameTextBack = null;

    [SerializeField]
    Text PauseMenuInfo = null;

    readonly Color ActiveColor = Color.white;

    readonly Color InactiveColor = new Color(0.745f, 0.745f, 0.745f, 1.0f);

    public delegate void AcceptResumeGame();
    public AcceptResumeGame acceptResumeGame;

    public delegate void AcceptRestart();
    public AcceptRestart acceptRestart;

    public delegate void AcceptOptions();
    public AcceptOptions acceptOptions;

    public delegate void AcceptQuitGame();
    public AcceptQuitGame acceptQuitGame;

    void Start()
    {
        SelectResume();
    }

    public void SelectResume()
    {
        ResumeGameText.color = ActiveColor;
        ResumeGameTextBack.SetActive(true);
        RestartText.color = InactiveColor;
        RestartTextBack.SetActive(false);
        OptionsText.color = InactiveColor;
        OptionsTextBack.SetActive(false);
        QuitGameText.color = InactiveColor;
        QuitGameTextBack.SetActive(false);

        PauseMenuInfo.text = "ゲームを再開します";
    }

    public void SelectRestart()
    {
        ResumeGameText.color = InactiveColor;
        ResumeGameTextBack.SetActive(false);
        RestartText.color = ActiveColor;
        RestartTextBack.SetActive(true);
        OptionsText.color = InactiveColor;
        OptionsTextBack.SetActive(false);
        QuitGameText.color = InactiveColor;
        QuitGameTextBack.SetActive(false);

        PauseMenuInfo.text = "ステージを最初からやり直します";
    }

    public void SelectOptions()
    {
        ResumeGameText.color = InactiveColor;
        ResumeGameTextBack.SetActive(false);
        RestartText.color = InactiveColor;
        RestartTextBack.SetActive(false);
        OptionsText.color = ActiveColor;
        OptionsTextBack.SetActive(true);
        QuitGameText.color = InactiveColor;
        QuitGameTextBack.SetActive(false);

        PauseMenuInfo.text = "ゲーム設定を変更します（未実装です）";
    }

    public void SelectQuitGame()
    {
        ResumeGameText.color = InactiveColor;
        ResumeGameTextBack.SetActive(false);
        RestartText.color = InactiveColor;
        RestartTextBack.SetActive(false);
        OptionsText.color = InactiveColor;
        OptionsTextBack.SetActive(false);
        QuitGameText.color = ActiveColor;
        QuitGameTextBack.SetActive(true);

        PauseMenuInfo.text = "タイトル画面に戻ります";
    }
}
