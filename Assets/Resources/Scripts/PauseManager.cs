﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;

public enum PauseMenuSelect
{
    Restart,
    ReturnMapScene,
}

public class PauseManager : SingletonMonoBehaviour<PauseManager>
{
    public bool Pausing { get; set; } = false;

    List<IDisposable> disposables = new List<IDisposable>();

    [SerializeField]
    GameObject pauseCanvas = null;

    [SerializeField]
    PauseMenu PauseMenu = null;

    int pauseMenuIndex = 0;

    void Start()
    {
        Initialize();
        ResumeAll();

        this.UpdateAsObservable()
            .Where(_ => !PlayerCharacterManager.Instance.Deaded.Value)
            .Where(_ => !FadeInOutManager.Instance.IsFadeNow.Value)
            .Where(_ => !InGameManager.Instance.IsEventPlaying.Value)
            .Where(_ => GameInput.Instance.GetButtonDown(GameKey.Pause))
            .Subscribe(_ =>
            {
                // ポーズ中に再度ポーズを押すと解除
                if (Pausing)
                {
                    AudioManager.PlaySe(GameSE.CursorCancel);
                    ResumeAll();
                }
                else
                {
                    AudioManager.PlaySe(GameSE.CursorEnter);
                    PauseAll();
                }
            })
            .AddTo(this);

        // キャンセルキーでポーズ解除
        this.UpdateAsObservable()
            .Where(_ => Pausing)
            .Where(_ => GameInput.Instance.GetButtonDown(GameKey.Cancel))
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.CursorCancel);
                ResumeAll();
            })
            .AddTo(this);

        this.UpdateAsObservable()
            .Where(_ => Pausing)
            .Subscribe(_ =>
            {
                var gameInput = GameInput.Instance;

                if (gameInput.GetDownArrowButtonDown())
                {
                    pauseMenuIndex++;
                    if (pauseMenuIndex > 3)
                    {
                        pauseMenuIndex = 0;
                    }

                    AudioManager.PlaySe(GameSE.MenuCursor);
                    SelectPauseMenu(pauseMenuIndex);
                }
                else if (gameInput.GetUpArrowButtonDown())
                {
                    pauseMenuIndex--;
                    if (pauseMenuIndex < 0)
                    {
                        pauseMenuIndex = 3;
                    }

                    AudioManager.PlaySe(GameSE.MenuCursor);
                    SelectPauseMenu(pauseMenuIndex);
                }

                if (gameInput.GetButtonDown(GameKey.Submit))
                {
                    AcceptTitleMenu(pauseMenuIndex);
                }
            });
    }

    void Initialize()
    {
        PauseMenu.acceptResumeGame += delegate ()
        {
            AudioManager.PlaySe(GameSE.CursorCancel);
            ResumeAll();
        };

        PauseMenu.acceptRestart += delegate ()
        {
            AudioManager.PlaySe(GameSE.CursorEnter);
            PlayerCharacterManager.Instance.Player.SetInvinsible(int.MaxValue);
            ResumeAll();
            AppManager.Instance.GameSceneReload();
        };

        PauseMenu.acceptOptions += delegate ()
        {
            Debug.Log("Options");
        };

        PauseMenu.acceptQuitGame += delegate ()
        {
            AudioManager.PlaySe(GameSE.CursorEnter);
            PlayerCharacterManager.Instance.Player.SetInvinsible(int.MaxValue);
            ResumeAll();
            AppManager.Instance.ChangeToTitleScene();
        };
    }

    void PauseAll()
    {
        Debug.Assert(!Pausing);
        Pausing = true;
        Time.timeScale = 0;

        pauseCanvas.gameObject.SetActive(true);

        PauseMenu.SelectResume();
    }

    void ResumeAll()
    {
        Time.timeScale = 1;
        Pausing = false;

        pauseCanvas.gameObject.SetActive(false);

        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }

    void SelectPauseMenu(int index)
    {
        switch (index)
        {
            case 0:
                PauseMenu.SelectResume();
                break;
            case 1:
                PauseMenu.SelectRestart();
                break;
            case 2:
                PauseMenu.SelectOptions();
                break;
            case 3:
                PauseMenu.SelectQuitGame();
                break;
        }
    }

    void AcceptTitleMenu(int index)
    {
        switch (index)
        {
            case 0:
                PauseMenu.acceptResumeGame();
                break;
            case 1:
                PauseMenu.acceptRestart();
                break;
            case 2:
                PauseMenu.acceptOptions();
                break;
            case 3:
                PauseMenu.acceptQuitGame();
                break;
        }
    }

    private void OnDestroy()
    {
        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }
}
