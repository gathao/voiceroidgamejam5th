﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.Tilemaps;

public class MapManager : MonoBehaviour
{
    [SerializeField]
    Map startMap = null;

    [SerializeField]
    GameObject playerSpawnPoint = null;

    public Map CurrentMap { get; private set; }

    Subject<Map> onMapEnterd = new Subject<Map>();

    public IObservable<Map> OnAreaEntered() { return onMapEnterd; }

    Subject<Map> onMapLeaved = new Subject<Map>();

    public IObservable<Map> OnAreaLeaved() { return onMapLeaved; }

    //[SerializeField]
    //GameBGM stageBGM = GameBGM.Stage001;

    // Start is called before the first frame update
    void Start()
    {
        AudioManager.PlayBgm(GameBGM.Stage);

        var player = PlayerCharacterManager.Instance.Player;

        // プレイヤーの初期位置を取得
        if (playerSpawnPoint == null)
        {
            Debug.LogError("PlayerSpawnPointがAreaManagerにセットされていません！確認して？");
            playerSpawnPoint = GameObject.Find("PlayerSpawnPoint");
        }
        if (playerSpawnPoint == null)
        {
            Debug.LogError("PlayerSpawnPointがステージ上に存在しないぞ！\nしかたないから初期座標は適当に(5, 5)にしとくぞ");
            player.transform.position = new Vector3(5, 5);
        }
        else
        {
            player.transform.position = playerSpawnPoint.transform.position;
        }

        ChangeArea(startMap);

        player.LookRight();
    }

    public void ChangeArea(Map map)
    {
        if (CurrentMap != null)
        {
            onMapLeaved.OnNext(CurrentMap);
        }
        CurrentMap = map;

        CurrentMap.EnterMap();
        onMapEnterd.OnNext(map);
    }
}
