﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UniRx.Triggers;
using UnityEngine.SceneManagement;

public class InGameManager : SingletonMonoBehaviour<InGameManager> {
    
    Subject<Unit> onGameSceneLoaded = new Subject<Unit>();
    public IObservable<Unit> OnGameSceneLoaded { get { return onGameSceneLoaded; } }
    
    // イベント中フラグ。
    // 色んなオブジェクトがイベント中か否かを参照してイベント中は動かないようにする。
    // あとはイベントが開始された瞬間にザコ敵消去するとか。
    public BoolReactiveProperty IsEventPlaying { get; } = new BoolReactiveProperty(false);

    // タイムカウント
    private float stageTimer;
    public float StageTimer { get { return stageTimer; } }

    // 実績用コイン
    private bool medalCoin;
    public bool MedalCoin { get { return medalCoin;} }

    // AppManagerでロードされたタイミングで呼び出される
    public void StartGameScene()
    {
        stageTimer = 0;
        this.FixedUpdateAsObservable()
            //.Where(_ => !PauseManager.Instance.Pausing)
            .Where(_ => !IsEventPlaying.Value)
            .Subscribe(_ =>
            {
                stageTimer += Time.deltaTime;
            });

        this.FixedUpdateAsObservable()
            .Take(1)
            .Subscribe(_ =>
            {
                onGameSceneLoaded.OnNext(Unit.Default);
                onGameSceneLoaded.OnCompleted();
            });
    }

    /// <summary>
    /// プレイヤー死亡時の処理
    /// </summary>
    public void PlayerDeaded()
    {
        // 死亡時、プレイヤーキャラのObserverを破棄
        PlayerCharacterManager.Instance.Dispose();
        PlayerCharacterManager.Instance.Player.SetInvinsible(10000);

        // 現在のエリアから脱出することでスポナー等を破棄
        //var areaManager = GameObject.Find("MainMap").GetComponent<AreaManager>();
        //if (areaManager != null)
        //{
        //    areaManager.CurrentAreaLeave();
        //}

        // 現在のステージをリロード
        AppManager.Instance.GameSceneReload();
    }

    /// <summary>
    /// 実績コイン取得フラグを立てる
    /// </summary>
    public void GetMedalCoin()
    {
        medalCoin = true;
    }

    /// <summary>
    /// ステージの規定タイム以内か
    /// </summary>
    /// <returns></returns>
    public bool IsClearTimeNormaAchieve()
    {
        return true;
        //float norma = StageMasterData.Instance[SaveDataManager.Instance.PlayingStageIndex].NormaTime;
        //return stageTimer <= norma;
    }

}
