﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public enum CameraMode
{
    Follow,
    Demo
}

[RequireComponent(typeof(Camera))]
public class GameCameraController : MonoBehaviour
{
    private Camera attackedCamera;

    public GameObject followTarget;

    [SerializeField] private float followTargetCorrectionY = 1f;

    private float shakeDecay = 0.04f;
    private float shakeIntensity;

    public CameraMode Mode { get; private set; }

    public float MinX { get; set; }
    public float MaxX { get; set; }
    public float MinY { get; set; }
    public float MaxY { get; set; }

    public Vector3 BasePosition { get; private set; }

    // カメラが各画面端か否か
    public BoolReactiveProperty IsLeftEdge { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty IsRightEdge { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty IsBottomEdge { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty IsUpEdge { get; set; } = new BoolReactiveProperty(false);

    public GameCameraController()
    {
        MinX = 0;
        MaxX = 1000;
        MinY = 0;
        MaxY = 1000;
    }

    private void Awake()
    {
        attackedCamera = GetComponent<Camera>();
        Mode = CameraMode.Follow;
    }

    private void Start()
    {
        this.LateUpdateAsObservable()
            .Where(_ => Mode == CameraMode.Follow && followTarget != null)
            .Subscribe(_ => {
                // カメラを遅れて追従させる
                transform.position = Vector3.Lerp(
                    transform.position,
                    new Vector3(followTarget.transform.position.x, followTarget.transform.position.y + followTargetCorrectionY, transform.position.z),
                    5.0f * Time.deltaTime
                );
            });

        // ステージにあわせてスクロール範囲を制御する
        // 左端チェック・補正
        this.LateUpdateAsObservable()
            .Select(_ => attackedCamera.ScreenToWorldPoint(new Vector3(0, 0, 0)))
            //.Where(camLeftPos => camLeftPos.x < MinX)
            .Subscribe(_ => {

                var camPosZero = attackedCamera.ScreenToWorldPoint(new Vector3(0, 0, 0));
                var camPosMax = attackedCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

                // 左端チェック・補正
                if (IsLeftEdge.Value = camPosZero.x <= MinX)
                {
                    attackedCamera.transform.position += new Vector3(MinX - camPosZero.x, 0, 0);
                    BasePosition = attackedCamera.transform.position;
                }
                // 右端チェック・補正
                if (IsRightEdge.Value = camPosMax.x >= MaxX)
                {
                    attackedCamera.transform.position -= new Vector3(camPosMax.x - MaxX, 0, 0);
                    BasePosition = attackedCamera.transform.position;
                }
                // 下端チェック・補正
                if (IsBottomEdge.Value = camPosZero.y <= MinY)
                {
                    attackedCamera.transform.position += new Vector3(0, MinY - camPosZero.y, 0);
                    BasePosition = attackedCamera.transform.position;
                }
                // 上端チェック・補正
                if (IsUpEdge.Value = camPosMax.y >= MaxY)
                {
                    attackedCamera.transform.position -= new Vector3(0, camPosMax.y - MaxY, 0);
                    BasePosition = attackedCamera.transform.position;
                }

                if (AppManager.Instance.CurrentSceneState == SceneState.Game)
                {
                    BackGroundManager.Instance.BackGroundUpdate();
                }

            });

        // シェイク前かつステージ端補正後のカメラのポジションを記憶しておく
        this.LateUpdateAsObservable()
            .Where(_ => Mode == CameraMode.Follow && followTarget != null)
            .Subscribe(_ => {
                BasePosition = attackedCamera.transform.position;
            });

        // カメラのシェイク
        this.LateUpdateAsObservable()
            .Where(_ => shakeIntensity > 0)
            .Select(_ => Random.insideUnitSphere * shakeIntensity)
            .Select(randomVec => new Vector3(randomVec.x, randomVec.y, 0))
            .Subscribe(randomVec => {
                transform.position += randomVec;
                shakeIntensity -= shakeDecay;
            });
    }

    /// <summary>
    /// デモモードに切り替え
    /// </summary>
    public void SetDemoMode()
    {
        Mode = CameraMode.Demo;
        followTarget = null;
    }

    /// <summary>
    /// フォローモードに切り替え
    /// </summary>
    /// <param name="target"></param>
    public void SetFollowMode()
    {
        Mode = CameraMode.Follow;
        followTarget = PlayerCharacterManager.Instance.Player.gameObject;
    }
    /// <summary>
    /// フォローモードに切り替え（対象を指定）
    /// </summary>
    /// <param name="target"></param>
    public void SetFollowMode(GameObject target)
    {
        Mode = CameraMode.Follow;
        followTarget = target;
    }

    public void Shake(float power = 0.3f)
    {
        shakeIntensity = power;
    }
    
}
