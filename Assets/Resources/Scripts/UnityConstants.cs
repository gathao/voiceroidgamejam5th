// This file is auto-generated. Modifications are not saved.

namespace UnityConstants
{
    public static class Tags
    {
        /// <summary>
        /// Name of tag 'Untagged'.
        /// </summary>
        public const string Untagged = "Untagged";
        /// <summary>
        /// Name of tag 'Respawn'.
        /// </summary>
        public const string Respawn = "Respawn";
        /// <summary>
        /// Name of tag 'Finish'.
        /// </summary>
        public const string Finish = "Finish";
        /// <summary>
        /// Name of tag 'EditorOnly'.
        /// </summary>
        public const string EditorOnly = "EditorOnly";
        /// <summary>
        /// Name of tag 'MainCamera'.
        /// </summary>
        public const string MainCamera = "MainCamera";
        /// <summary>
        /// Name of tag 'Player'.
        /// </summary>
        public const string Player = "Player";
        /// <summary>
        /// Name of tag 'GameController'.
        /// </summary>
        public const string GameController = "GameController";
    }

    public static class SortingLayers
    {
        /// <summary>
        /// ID of sorting layer 'Default'.
        /// </summary>
        public const int Default = 0;
        /// <summary>
        /// ID of sorting layer 'Gackground'.
        /// </summary>
        public const int Gackground = 415331491;
        /// <summary>
        /// ID of sorting layer 'Ground'.
        /// </summary>
        public const int Ground = -1009772003;
        /// <summary>
        /// ID of sorting layer 'Enemy'.
        /// </summary>
        public const int Enemy = 1686910271;
        /// <summary>
        /// ID of sorting layer 'EnemyBeam'.
        /// </summary>
        public const int EnemyBeam = -803420371;
        /// <summary>
        /// ID of sorting layer 'Player'.
        /// </summary>
        public const int Player = 1261018993;
        /// <summary>
        /// ID of sorting layer 'PlayerBeam'.
        /// </summary>
        public const int PlayerBeam = 806123509;
        /// <summary>
        /// ID of sorting layer 'Item'.
        /// </summary>
        public const int Item = 1234777931;
    }

    public static class Layers
    {
        /// <summary>
        /// Index of layer 'Default'.
        /// </summary>
        public const int Default = 0;
        /// <summary>
        /// Index of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFX = 1;
        /// <summary>
        /// Index of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_Raycast = 2;
        /// <summary>
        /// Index of layer 'Water'.
        /// </summary>
        public const int Water = 4;
        /// <summary>
        /// Index of layer 'UI'.
        /// </summary>
        public const int UI = 5;
        /// <summary>
        /// Index of layer 'Ground'.
        /// </summary>
        public const int Ground = 8;
        /// <summary>
        /// Index of layer 'EnemyAttack'.
        /// </summary>
        public const int EnemyAttack = 9;
        /// <summary>
        /// Index of layer 'EnemyKuraiHitbox'.
        /// </summary>
        public const int EnemyKuraiHitbox = 10;
        /// <summary>
        /// Index of layer 'PlayerAttack'.
        /// </summary>
        public const int PlayerAttack = 11;
        /// <summary>
        /// Index of layer 'PlayerKuraiHitbox'.
        /// </summary>
        public const int PlayerKuraiHitbox = 12;
        /// <summary>
        /// Index of layer 'CameraBoundingBox'.
        /// </summary>
        public const int CameraBoundingBox = 13;

        /// <summary>
        /// Bitmask of layer 'Default'.
        /// </summary>
        public const int DefaultMask = 1 << 0;
        /// <summary>
        /// Bitmask of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFXMask = 1 << 1;
        /// <summary>
        /// Bitmask of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_RaycastMask = 1 << 2;
        /// <summary>
        /// Bitmask of layer 'Water'.
        /// </summary>
        public const int WaterMask = 1 << 4;
        /// <summary>
        /// Bitmask of layer 'UI'.
        /// </summary>
        public const int UIMask = 1 << 5;
        /// <summary>
        /// Bitmask of layer 'Ground'.
        /// </summary>
        public const int GroundMask = 1 << 8;
        /// <summary>
        /// Bitmask of layer 'EnemyAttack'.
        /// </summary>
        public const int EnemyAttackMask = 1 << 9;
        /// <summary>
        /// Bitmask of layer 'EnemyKuraiHitbox'.
        /// </summary>
        public const int EnemyKuraiHitboxMask = 1 << 10;
        /// <summary>
        /// Bitmask of layer 'PlayerAttack'.
        /// </summary>
        public const int PlayerAttackMask = 1 << 11;
        /// <summary>
        /// Bitmask of layer 'PlayerKuraiHitbox'.
        /// </summary>
        public const int PlayerKuraiHitboxMask = 1 << 12;
        /// <summary>
        /// Bitmask of layer 'CameraBoundingBox'.
        /// </summary>
        public const int CameraBoundingBoxMask = 1 << 13;
    }

    public static class Scenes
    {
        /// <summary>
        /// ID of scene 'GameBaseScene'.
        /// </summary>
        public const int GameBaseScene = 0;
        /// <summary>
        /// ID of scene 'TitleScene'.
        /// </summary>
        public const int TitleScene = 1;
        /// <summary>
        /// ID of scene 'InGameScene'.
        /// </summary>
        public const int InGameScene = 2;
        /// <summary>
        /// ID of scene 'StageScene'.
        /// </summary>
        public const int StageScene = 3;
    }

    public static class SceneNames
    {
        public static readonly string GameBaseScene = @"GameBaseScene";
        public static readonly string TitleScene = @"TitleScene";
        public static readonly string InGameScene = @"InGameScene";
        public static readonly string StageScene = @"StageScene";
    }

    public static class Axes
    {
        /// <summary>
        /// Input axis 'Horizontal'.
        /// </summary>
        public const string Horizontal = "Horizontal";
        /// <summary>
        /// Input axis 'Vertical'.
        /// </summary>
        public const string Vertical = "Vertical";
        /// <summary>
        /// Input axis 'Fire1'.
        /// </summary>
        public const string Fire1 = "Fire1";
        /// <summary>
        /// Input axis 'Fire2'.
        /// </summary>
        public const string Fire2 = "Fire2";
        /// <summary>
        /// Input axis 'Fire3'.
        /// </summary>
        public const string Fire3 = "Fire3";
        /// <summary>
        /// Input axis 'Jump'.
        /// </summary>
        public const string Jump = "Jump";
        /// <summary>
        /// Input axis 'Mouse X'.
        /// </summary>
        public const string Mouse_X = "Mouse X";
        /// <summary>
        /// Input axis 'Mouse Y'.
        /// </summary>
        public const string Mouse_Y = "Mouse Y";
        /// <summary>
        /// Input axis 'Mouse ScrollWheel'.
        /// </summary>
        public const string Mouse_ScrollWheel = "Mouse ScrollWheel";
        /// <summary>
        /// Input axis 'Submit'.
        /// </summary>
        public const string Submit = "Submit";
        /// <summary>
        /// Input axis 'Cancel'.
        /// </summary>
        public const string Cancel = "Cancel";
    }
}

