﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public interface IInGameInput
{
    // 各ボタンの状態
    bool GetButton(GameKey gameKey);
    bool GetButtonDown(GameKey gameKey);
    bool GetButtonUp(GameKey gameKey);

    // 十字キーが押されている状態
    bool GetRightArrowButton();
    bool GetLeftArrowButton();
    bool GetUpArrowButton();
    bool GetDownArrowButton();

    // 十字キーが押された瞬間
    bool GetRightArrowButtonDown();
    bool GetLeftArrowButtonDown();
    bool GetUpArrowButtonDown();
    bool GetDownArrowButtonDown();

    // キー入力状態を更新
    void InputUpdate();
}

public class KeyboardAndGamePadInput : IInGameInput
{
    bool isRight = false;
    bool isLeft = false;
    bool isUp = false;
    bool isDown = false;

    bool isRightArrowDown = false;
    bool isLeftArrowDown = false;
    bool isUpArrowDown = false;
    bool isDownArrowDown = false;

    public bool GetButton(GameKey gameKey)
    {
        if (gameKey == GameKey.Submit)
        {
            gameKey = GameKey.Action1;
        }
        if (gameKey == GameKey.Cancel)
        {
            gameKey = GameKey.Action2;
        }

        var key = GameInput.Instance.GetKeyboardKeyconfig(gameKey);
        var pad = GameInput.Instance.GetGamepadKeyconfig(gameKey);
        return Input.GetKey(key) || Input.GetKey(pad);
    }
    public bool GetButtonDown(GameKey gameKey)
    {
        if (gameKey == GameKey.Submit)
        {
            gameKey = GameKey.Action1;
        }
        if (gameKey == GameKey.Cancel)
        {
            gameKey = GameKey.Action2;
        }

        var key = GameInput.Instance.GetKeyboardKeyconfig(gameKey);
        var pad = GameInput.Instance.GetGamepadKeyconfig(gameKey);
        return Input.GetKeyDown(key) || Input.GetKeyDown(pad);
    }
    public bool GetButtonUp(GameKey gameKey)
    {
        if (gameKey == GameKey.Submit)
        {
            gameKey = GameKey.Action1;
        }
        if (gameKey == GameKey.Cancel)
        {
            gameKey = GameKey.Action2;
        }

        var key = GameInput.Instance.GetKeyboardKeyconfig(gameKey);
        var pad = GameInput.Instance.GetGamepadKeyconfig(gameKey);
        return Input.GetKeyUp(key) || Input.GetKeyUp(pad);
    }

    // 矢印キー
    public bool GetLeftArrowButton()
    {
        return Input.GetAxisRaw("Horizontal") < -0.9;
    }
    public bool GetRightArrowButton()
    {
        return Input.GetAxisRaw("Horizontal") > 0.9;
    }
    public bool GetUpArrowButton()
    {
        return Input.GetAxisRaw("Vertical") > 0.9;
    }
    public bool GetDownArrowButton()
    {
        return Input.GetAxisRaw("Vertical") < -0.9;
    }

    public bool GetRightArrowButtonDown()
    {
        return isRightArrowDown;
    }
    public bool GetLeftArrowButtonDown()
    {
        return isLeftArrowDown;
    }
    public bool GetUpArrowButtonDown()
    {
        return isUpArrowDown;
    }
    public bool GetDownArrowButtonDown()
    {
        return isDownArrowDown;
    }

    public void InputUpdate()
    {
        if (GetRightArrowButton())
        {
            if (isRight)
            {
                isRightArrowDown = false;
            }
            else
            {
                isRightArrowDown = true;
                isRight = true;
            }
        }
        else
        {
            isRightArrowDown = false;
            isRight = false;
        }

        if (GetLeftArrowButton())
        {
            if (isLeft)
            {
                isLeftArrowDown = false;
            }
            else
            {
                isLeftArrowDown = true;
                isLeft = true;
            }
        }
        else
        {
            isLeftArrowDown = false;
            isLeft = false;
        }

        if (GetUpArrowButton())
        {
            if (isUp)
            {
                isUpArrowDown = false;
            }
            else
            {
                isUpArrowDown = true;
                isUp = true;
            }
        }
        else
        {
            isUpArrowDown = false;
            isUp = false;
        }

        if (GetDownArrowButton())
        {
            if (isDown)
            {
                isDownArrowDown = false;
            }
            else
            {
                isDownArrowDown = true;
                isDown = true;
            }
        }
        else
        {
            isDownArrowDown = false;
            isDown = false;
        }
    }
}

public class DemoInput : IInGameInput
{
    // デモ中に左右入力を入れ続けるフラグ
    public bool RightArrowButton { get; internal set; }
    public bool LeftArrowButton { get; internal set; }

    // キー（デモ中は決定・キャンセルキーのみ、キー入力を受け付ける）
    public bool GetButton(GameKey gameKey)
    {
        return false;
    }
    public bool GetButtonUp(GameKey gameKey)
    {
        return false;
    }
    public bool GetButtonDown(GameKey gameKey)
    {
        if (gameKey == GameKey.Submit)
        {
            gameKey = GameKey.Action1;

            var key = GameInput.Instance.GetKeyboardKeyconfig(gameKey);
            var pad = GameInput.Instance.GetGamepadKeyconfig(gameKey);
            return Input.GetKeyDown(key) || Input.GetKeyDown(pad);
        }
        else if (gameKey == GameKey.Pause)
        {
            gameKey = GameKey.Pause;
            var key = GameInput.Instance.GetKeyboardKeyconfig(gameKey);
            var pad = GameInput.Instance.GetGamepadKeyconfig(gameKey);
            return Input.GetKeyDown(key) || Input.GetKeyDown(pad);
        }
        else
        {
            return false;
        }
    }

    // 矢印キー
    public bool GetLeftArrowButton() => LeftArrowButton;
    public bool GetRightArrowButton() => RightArrowButton;
    public bool GetUpArrowButton() => false;
    public bool GetDownArrowButton() => false;

    public bool GetRightArrowButtonDown() => false;
    public bool GetLeftArrowButtonDown() => false;
    public bool GetUpArrowButtonDown() => false;
    public bool GetDownArrowButtonDown() => false;

    public void InputUpdate() { }
}

public enum GameKey
{
    Action1,
    Action2,
    Jump,
    Camera,
    Pause,
    Submit,
    Cancel,
}

public class GameInput : SingletonMonoBehaviour<GameInput>, IInGameInput
{
    IInGameInput currentInput;
    Dictionary<GameKey, KeyCode> keyboardKeyconfig = new Dictionary<GameKey, KeyCode>();
    Dictionary<GameKey, KeyCode> gamepadKeyconfig = new Dictionary<GameKey, KeyCode>();

    public KeyCode GetKeyboardKeyconfig(GameKey keyName)
    {
        return keyboardKeyconfig[keyName];
    }
    public KeyCode GetGamepadKeyconfig(GameKey keyName)
    {
        return keyboardKeyconfig[keyName];
        //return SaveDataManager.Instance.GetPadConfig(keyName);
    }

    protected override void Awake()
    {
        base.Awake();
        ChangeNormalInput();
    }

    void Start()
    {
        keyboardKeyconfig[GameKey.Action1] = KeyCode.Z;
        keyboardKeyconfig[GameKey.Action2] = KeyCode.X;
        keyboardKeyconfig[GameKey.Jump] = KeyCode.Space;
        keyboardKeyconfig[GameKey.Camera] = KeyCode.V;
        keyboardKeyconfig[GameKey.Pause] = KeyCode.Escape;
        keyboardKeyconfig[GameKey.Submit] = keyboardKeyconfig[GameKey.Action1];
        keyboardKeyconfig[GameKey.Cancel] = keyboardKeyconfig[GameKey.Action2];

        gamepadKeyconfig[GameKey.Action1] = KeyCode.Z;
        gamepadKeyconfig[GameKey.Action2] = KeyCode.X;
        gamepadKeyconfig[GameKey.Jump] = KeyCode.Space;
        gamepadKeyconfig[GameKey.Camera] = KeyCode.V;
        gamepadKeyconfig[GameKey.Pause] = KeyCode.Escape;
        gamepadKeyconfig[GameKey.Submit] = gamepadKeyconfig[GameKey.Action1];
        gamepadKeyconfig[GameKey.Cancel] = gamepadKeyconfig[GameKey.Action2];
    }

    private void Update()
    {
        InputUpdate();
    }

    /// <summary>インプットを通常モードに切り替え</summary>
    public KeyboardAndGamePadInput ChangeNormalInput()
    {
        currentInput = new KeyboardAndGamePadInput();
        return currentInput as KeyboardAndGamePadInput;
    }

    /// <summary>インプットをデモモードに切り替え</summary>
    public DemoInput ChangeDemoInput()
    {
        if (currentInput is DemoInput)
        {
            return currentInput as DemoInput;
        }
        currentInput = new DemoInput();
        return currentInput as DemoInput;
    }

    public bool GetButton(GameKey gameKey) => currentInput.GetButton(gameKey);
    public bool GetButtonDown(GameKey gameKey) => currentInput.GetButtonDown(gameKey);
    public bool GetButtonUp(GameKey gameKey) => currentInput.GetButtonUp(gameKey);

    public bool GetLeftArrowButton() => currentInput.GetLeftArrowButton();
    public bool GetRightArrowButton() => currentInput.GetRightArrowButton();
    public bool GetUpArrowButton() => currentInput.GetUpArrowButton();
    public bool GetDownArrowButton() => currentInput.GetDownArrowButton();

    public bool GetRightArrowButtonDown() => currentInput.GetRightArrowButtonDown();
    public bool GetLeftArrowButtonDown() => currentInput.GetLeftArrowButtonDown();
    public bool GetUpArrowButtonDown() => currentInput.GetUpArrowButtonDown();
    public bool GetDownArrowButtonDown() => currentInput.GetDownArrowButtonDown();

    public void InputUpdate() => currentInput.InputUpdate();
}
