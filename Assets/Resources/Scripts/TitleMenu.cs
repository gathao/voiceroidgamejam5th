﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleMenu : MonoBehaviour
{
    [SerializeField]
    Text GameStartText = null;

    [SerializeField]
    GameObject GameStartTextBack = null;

    [SerializeField]
    Text OptionsText = null;

    [SerializeField]
    GameObject OptionsTextBack = null;

    [SerializeField]
    Text ExitGameText = null;

    [SerializeField]
    GameObject ExitGameTextBack = null;

    readonly Color ActiveColor = Color.white;

    readonly Color InactiveColor = new Color(0.745f, 0.745f, 0.745f, 1.0f);

    public delegate void AcceptGameStart();
    public AcceptGameStart acceptGameStart;

    public delegate void AcceptOptions();
    public AcceptOptions acceptOptions;

    public delegate void AcceptExitGame();
    public AcceptExitGame acceptExitGame;

    void Start()
    {
        SelectGameStart();
    }

    public void SelectGameStart()
    {
        GameStartText.color = ActiveColor;
        GameStartTextBack.SetActive(true);
        OptionsText.color = InactiveColor;
        OptionsTextBack.SetActive(false);
        ExitGameText.color = InactiveColor;
        ExitGameTextBack.SetActive(false);
    }

    public void SelectOptions()
    {
        GameStartText.color = InactiveColor;
        GameStartTextBack.SetActive(false);
        OptionsText.color = ActiveColor;
        OptionsTextBack.SetActive(true);
        ExitGameText.color = InactiveColor;
        ExitGameTextBack.SetActive(false);
    }

    public void SelectExitGame()
    {
        GameStartText.color = InactiveColor;
        GameStartTextBack.SetActive(false);
        OptionsText.color = InactiveColor;
        OptionsTextBack.SetActive(false);
        ExitGameText.color = ActiveColor;
        ExitGameTextBack.SetActive(true);
    }
}
