﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;
using UnityConstants;

public class TitleManager : MonoBehaviour
{
    [SerializeField]
    Text PreeseAnyKeyText = null;

    [SerializeField]
    TitleMenu TitleMenu = null;

    [SerializeField]
    Text KeyInfoText = null;

    enum TitleMenuState
    {
        PreeseAnyKey,
        TitleMenu,
    }

    TitleMenuState currentState = TitleMenuState.PreeseAnyKey;

    int titleMenuIndex = 0;

    float preeseAnyKeyTextDurasionSecond = 1.0f;

    void Start()
    {
        Initialize();

        AudioManager.PlayBgm(GameBGM.Title);

        // 「Preese Any Key」をゆっくり点滅
        PreeseAnyKeyText.DOFade(0.0f, this.preeseAnyKeyTextDurasionSecond)
            .SetEase(Ease.InCubic)
            .SetLoops(-1, LoopType.Yoyo);

        this.UpdateAsObservable()
            .Where(_ => currentState == TitleMenuState.PreeseAnyKey)
            .Where(_ =>
                GameInput.Instance.GetButtonDown(GameKey.Action1) ||
                GameInput.Instance.GetButtonDown(GameKey.Action2) ||
                GameInput.Instance.GetButtonDown(GameKey.Submit) ||
                GameInput.Instance.GetButtonDown(GameKey.Cancel) ||
                GameInput.Instance.GetButtonDown(GameKey.Jump) ||
                GameInput.Instance.GetButtonDown(GameKey.Pause)
            )
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.CursorEnter);

                // 「Preese Any Key」がビカビカしてからメニューに移る
                PreeseAnyKeyText.DOFade(0.0f, this.preeseAnyKeyTextDurasionSecond / 20.0f)
                    .SetEase(Ease.InCubic)
                    .SetLoops(10, LoopType.Yoyo)
                    .OnComplete(() =>
                    {
                        currentState = TitleMenuState.TitleMenu;
                        PreeseAnyKeyText.gameObject.SetActive(false);
                        TitleMenu.gameObject.SetActive(true);
                        KeyInfoText.gameObject.SetActive(true);
                        TitleMenu.SelectGameStart();
                    })
                    .Play();
            });

        this.UpdateAsObservable()
            .Where(_ => currentState == TitleMenuState.TitleMenu)
            .Subscribe(_ =>
            {
                var gameInput = GameInput.Instance;

                if (gameInput.GetDownArrowButtonDown())
                {
                    titleMenuIndex++;
                    if (titleMenuIndex > 2)
                    {
                        titleMenuIndex = 0;
                    }

                    AudioManager.PlaySe(GameSE.MenuCursor);
                    SelectTitleMenu(titleMenuIndex);
                }
                else if(gameInput.GetUpArrowButtonDown())
                {
                    titleMenuIndex--;
                    if (titleMenuIndex < 0)
                    {
                        titleMenuIndex = 2;
                    }

                    AudioManager.PlaySe(GameSE.MenuCursor);
                    SelectTitleMenu(titleMenuIndex);
                }

                if(gameInput.GetButtonDown(GameKey.Submit))
                {
                    AcceptTitleMenu(titleMenuIndex);
                }
            });
    }

    void Initialize()
    {
        PreeseAnyKeyText.gameObject.SetActive(true);
        TitleMenu.gameObject.SetActive(false);
        KeyInfoText.gameObject.SetActive(false);

        TitleMenu.acceptGameStart += delegate() 
        {
            AudioManager.PlaySe(GameSE.CursorEnter);
            AppManager.Instance.ChangeToInGameScene(SceneNames.StageScene);
        };

        TitleMenu.acceptOptions += delegate ()
        {
            Debug.Log("Options");
        };

        TitleMenu.acceptExitGame += delegate ()
        {
            AudioManager.PlaySe(GameSE.CursorEnter);
            ExitGame();
        };
    }

    void SelectTitleMenu(int index)
    {
        switch(index)
        {
            case 0:
                TitleMenu.SelectGameStart();
                break;
            case 1:
                TitleMenu.SelectOptions();
                break;
            case 2:
                TitleMenu.SelectExitGame();
                break;
        }
    }

    void AcceptTitleMenu(int index)
    {
        switch (index)
        {
            case 0:
                TitleMenu.acceptGameStart();
                break;
            case 1:
                TitleMenu.acceptOptions();
                break;
            case 2:
                TitleMenu.acceptExitGame();
                break;
        }
    }

    void ExitGame()
    {
#if UNITY_EDITOR

        UnityEditor.EditorApplication.isPlaying = false;

#elif UNITY_STANDALONE

        UnityEngine.Application.Quit();

# endif
    }
}
