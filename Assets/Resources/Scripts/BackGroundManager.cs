﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BackgroundImageDistance
{
    Far,
    Near,
    Foreground,
}

public class StageBackgroundParam
{
    public Sprite sprite;
    public float Xdistance = 200.0f;
    public float Ydistance = 220.0f;
    public float Xoffset = 0.0f;
    public float Yoffset = 0.665f;
    public float Xscale = 5.0f;
}

public class BackGroundManager : SingletonMonoBehaviour<BackGroundManager>
{
    [SerializeField] StageBackgroundTexture backgroundTexture = null;

    Dictionary<BackgroundImageDistance, StageBackgroundTexture> dic;

    public void ChangeSprite(BackgroundImageDistance distance, StageBackgroundParam param)
    {
        if (dic == null)
        {
            dic = new Dictionary<BackgroundImageDistance, StageBackgroundTexture>();

            backgroundTexture.Xdistance = param.Xdistance;
            backgroundTexture.Ydistance = param.Ydistance;
            backgroundTexture.XOffset = param.Xoffset;
            backgroundTexture.YOffset = param.Yoffset;
            backgroundTexture.transform.SetLocalScaleX(param.Xscale);
            dic.Add(BackgroundImageDistance.Far, backgroundTexture);
        }

        //if (sprite == null)
        //{
        //    Hide(distance);
        //}
        //else
        //{
        //    Show(distance);
        //    dic[distance].SetSprite(sprite);
        //}
    }
    public void ChangeScrollType(BackgroundImageDistance distance, AreaCameraType areaCameraType)
    {
        if (dic[distance] != null)
        {
            dic[distance].SetScrollType(areaCameraType);
        }
    }

    public void BackGroundUpdate()
    {
        if (dic == null)
        {
            return;
        }
        foreach (var item in dic)
        {
            if (item.Value != null)
            {
                item.Value.UpdatePosition();
            }
        }
    }

    void Show(BackgroundImageDistance distance)
    {
        Debug.Log("show");
        if (dic[distance] != null)
        {
            dic[distance].gameObject.SetActive(true);
        }
    }

    void Hide(BackgroundImageDistance distance)
    {
        Debug.Log("hide");
        if (dic[distance] != null)
        {
            dic[distance].gameObject.SetActive(false);
        }
    }
}
