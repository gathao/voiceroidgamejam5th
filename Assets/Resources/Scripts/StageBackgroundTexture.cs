﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class StageBackgroundTexture : MonoBehaviour
{
    Material material;

    [SerializeField]
    [Tooltip("物体までの距離。値が大きいほど遠くにある＝スクロールが遅くなる")]
    public float Xdistance = 50f;

    [SerializeField]
    [Tooltip("物体までの距離。値が大きいほど遠くにある＝スクロールが遅くなる")]
    public float Ydistance = 50f;

    [SerializeField]
    public float XOffset = 0.0f;

    [SerializeField]
    public float YOffset = 0.5f;

    Camera targetCamera;
    Vector3 offset;

    // カメラに対する背景の描画方法
    // HorizontalMove   : Y軸位置を固定する
    // VerticalMove     : X軸位置を固定する
    // BothMove         : 座標を固定しない（常にカメラを追従する）
    AreaCameraType cameraType;

    Vector2 textureOffset
    {
        get { return material.mainTextureOffset; }
        set { material.mainTextureOffset = value; }
    }

    private void Awake()
    {
        var meshRenderer = GetComponent<MeshRenderer>();
        material = Instantiate(meshRenderer.material);

        meshRenderer.material = material;

        targetCamera = Camera.main;
        offset = transform.position;
    }

    public void UpdatePosition()
    {
        var pos = targetCamera.transform.position + offset;

        switch (cameraType)
        {
            case AreaCameraType.HorizontalMove:
                transform.position = new Vector3(pos.x, 4f, transform.position.z);
                textureOffset = new Vector2(pos.x / Xdistance, 0.0f);
                break;
            case AreaCameraType.VerticalMove:
                transform.position = new Vector3(-2f, pos.y, transform.position.z);
                textureOffset = new Vector2(pos.x / Xdistance, 0.0f);
                break;
            case AreaCameraType.BothMove:
                transform.position = new Vector3(pos.x, pos.y, transform.position.z);
                textureOffset = new Vector2((pos.x / Xdistance) + XOffset, (pos.y / Ydistance) + YOffset);
                break;
            default:
                break;
        }
    }


    public void SetSprite(Sprite sprite)
    {
        material.mainTexture = sprite.texture;
    }

    public void SetScrollType(AreaCameraType areaCameraType)
    {
        cameraType = areaCameraType;
    }
}
