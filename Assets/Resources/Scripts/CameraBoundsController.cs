﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityConstants;

public class CameraBoundsController : MonoBehaviour
{
    [SerializeField]
    Transform cameraTop = null;

    [SerializeField]
    Transform cameraLeft = null;

    [SerializeField]
    Transform cameraRight = null;

    [SerializeField]
    Transform cameraBottom = null;

    [SerializeField]
    BoxCollider2D areaCollider01 = null;

    [SerializeField]
    BoxCollider2D areaCollider02 = null;

    [SerializeField]
    BoxCollider2D areaCollider03 = null;

    PlayerController player;

    MapManager mapManager;

    // Start is called before the first frame update
    void Start()
    {
        this.player = PlayerCharacterManager.Instance.Player;
        this.mapManager = FindObjectOfType<MapManager>();

        areaCollider01
            .OnTriggerEnter2DAsObservable()
            .Where(col => col.gameObject.layer == Layers.PlayerKuraiHitbox)
            .Subscribe(_ =>
            {
                cameraTop.position = new Vector3(0, 29, 0);
                cameraLeft.position = new Vector3(0, 0, 0);
                cameraRight.position = new Vector3(48, 0, 0);
                cameraBottom.position = new Vector3(0, -2, 0);

                mapManager.CurrentMap.SetupCamera();
            });

        areaCollider02
            .OnTriggerEnter2DAsObservable()
            .Where(col => col.gameObject.layer == Layers.PlayerKuraiHitbox)
            .Subscribe(_ =>
            {
                cameraTop.position = new Vector3(0, 29, 0);
                cameraLeft.position = new Vector3(8, 0, 0);
                cameraRight.position = new Vector3(55, 0, 0);
                cameraBottom.position = new Vector3(0, -30, 0);

                mapManager.CurrentMap.SetupCamera();
            });

        areaCollider03
            .OnTriggerEnter2DAsObservable()
            .Where(col => col.gameObject.layer == Layers.PlayerKuraiHitbox)
            .Subscribe(_ =>
            {
                cameraTop.position = new Vector3(0, 32, 0);
                cameraLeft.position = new Vector3(50, 0, 0);
                cameraRight.position = new Vector3(72, 0, 0);
                cameraBottom.position = new Vector3(0, 20, 0);

                mapManager.CurrentMap.SetupCamera();
            });
    }
}
