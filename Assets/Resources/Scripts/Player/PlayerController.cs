﻿using AIE2D;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityConstants;
using UnityEngine;
using DG.Tweening;

public enum PlayerAnimationState
{
    Wait,
    Walk,
    Jump,
    Fall,
    KnockBack,
    Dead,
    SpaceJump,
    Finish,
}

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D rigidBody2d = null;

    public Rigidbody2D RigidBody2d
    {
        get
        {
            return this.rigidBody2d;
        }
    }

    public Vector3 CentorPos
    {
        get
        {
            return transform.position + new Vector3(0.0f, 0.8f, 0.0f);
        }
    }

    [SerializeField]
    Collider2D groundCheckCollider = null;

    // すり抜け床にめり込んでいる判定
    [SerializeField]
    BoxCollider2D throughGroundCheckCollider = null;

    bool throughGroundTouching = false;

    // 地上状態・空中状態などのステートマシン
    public PlayerMoveStateMachine MoveStateMachine { get; } = new PlayerMoveStateMachine();

    // 攻撃の種類
    public PlayerBeamWeaponStateMachine BeamWeaponStateMachine { get; } = new PlayerBeamWeaponStateMachine();

    // ジャンプ可能かどうかのフラグ
    public readonly BoolReactiveProperty ExistsLandableGroundUnderFeet = new BoolReactiveProperty(false);

    // 移動可能かどうかのフラグ
    public readonly BoolReactiveProperty Walkable = new BoolReactiveProperty(true);

    // キャラクターが右を向いているか否か
    public readonly BoolReactiveProperty FacingRight = new BoolReactiveProperty(true);

    [SerializeField]
    float moveSpeedX = 4f;

    public float MoveSpeedX
    {
        get
        {
            return moveSpeedX;
        }
        protected set
        {
            moveSpeedX = value;
        }
    }

    [SerializeField]
    float defaultJumpPower = 400f;
    public float JumpPower { get; set; }

    // ダメージ受けたときの無敵時間
    int InvincibleTime = 60;

    // 無敵状態か否か
    public readonly BoolReactiveProperty IsInvinsible = new BoolReactiveProperty(false);

    // 残り無敵時間
    public readonly IntReactiveProperty InvinsibleTimer = new IntReactiveProperty(0);

    // ショートジャンプ入力受付フレーム
    protected int ShortJumpableFrame = 25;

    // ジャンプしたとき発火されるObserver
    Subject<Unit> onJump = new Subject<Unit>();
    public IObservable<Unit> OnJump { get { return onJump; } }

    protected List<IDisposable> disposables = new List<IDisposable>();

    PlayerAnimationState currentAnimeState = PlayerAnimationState.Wait;

    [SerializeField]
    Pausable pausable = null;

    [SerializeField]
    SpriteRenderer waitTexture = null;

    [SerializeField]
    SpriteRenderer walkTexture1 = null;

    [SerializeField]
    SpriteRenderer walkTexture2 = null;

    [SerializeField]
    SpriteRenderer walkTexture3 = null;

    [SerializeField]
    SpriteRenderer walkTexture4 = null;

    [SerializeField]
    SpriteRenderer spaceJumpSprite = null;

    DynamicAfterImageEffect2DPlayer spaceJumpMotionBlur;

    [SerializeField]
    SpriteRenderer damageSprite = null;

    [SerializeField]
    SpriteRenderer finishSprite = null;

    [SerializeField]
    GameObject shotSparkTexture = null;

    Coroutine shotSparkCoroutine = null;

    Coroutine walkAnimCoroutine = null;

    Coroutine deadAnimCoroutine = null;

    bool canSpaceJump = false;

    public bool CanSpaceJump
    {
        get
        {
            return this.canSpaceJump;
        }
        set
        {
            this.canSpaceJump = value;
        }
    }

    public bool isPlayerble { get; set; } = true;

    void Start()
    {
        spaceJumpMotionBlur = gameObject.AddComponent<DynamicAfterImageEffect2DPlayer>();
        spaceJumpMotionBlur.CreateAfterImage(spaceJumpSprite, 0.04f, 3);

        this.waitTexture.gameObject.SetActive(true);
        this.spaceJumpSprite.gameObject.SetActive(true);

        this.JumpPower = defaultJumpPower;

        this.MoveStateMachine.SetUp(this);
        this.MoveStateMachine.Change(PlayerMoveState.Air);

        this.BeamWeaponStateMachine.SetUp(this);
        this.BeamWeaponStateMachine.Change(PlayerBeamWeaponState.NormalBeam);
        //this.BeamWeaponStateMachine.Change(PlayerBeamWeaponState.PlasmaBeam);
        //this.BeamWeaponStateMachine.Change(PlayerBeamWeaponState.WaveBeam);

        // 着地判定の更新
        this.FixedUpdateAsObservable()
            .ObserveEveryValueChanged(_ =>
            {
                if (!isPlayerble)
                {
                    return false;
                }

                if (groundCheckCollider == null)
                {
                    return false;
                }
                else
                {
                    return groundCheckCollider.IsTouchingLayers(Layers.GroundMask);
                }
            })
            .Subscribe(val =>
            {
                ExistsLandableGroundUnderFeet.Value = val;
            })
            .AddTo(disposables);

        // 回転する
        this.FixedUpdateAsObservable()
            .Where(_ => this.currentAnimeState == PlayerAnimationState.SpaceJump)
            .Subscribe(_ =>
            {
                float dir = 1.0f;
                float spinSpeed = 40.0f;
                var rotate = spinSpeed * dir;
                spaceJumpSprite.transform.Rotate(0.0f, 0.0f, rotate);
            })
            .AddTo(disposables);

        // 無敵中なら無敵タイマーを毎フレームデクリメント
        // タイマーが0になったら無敵フラグをfalseに戻す
        this.FixedUpdateAsObservable()
            .Where(_ => IsInvinsible.Value)
            .Subscribe(_ =>
            {
                InvinsibleTimer.Value--;
                if (InvinsibleTimer.Value <= 0)
                {
                    // 死んだ後に無敵が解除されるとおかしなことになるので
                    // 条件分岐で制御する
                    if (!PlayerCharacterManager.Instance.Deaded.Value)
                    {
                        IsInvinsible.Value = false;
                    }
                }
            }).AddTo(disposables);
    }

    public void LookRight()
    {
        if (FacingRight.Value)
        {
            return;
        }
        FacingRight.Value = true;
        var tmp = transform.localScale;
        tmp.x = 1;
        transform.SetLocalScaleX(-transform.localScale.x);
    }
    public void LookLeft()
    {
        if (!FacingRight.Value)
        {
            return;
        }
        FacingRight.Value = false;
        var tmp = transform.localScale;
        tmp.x = -1;
        transform.SetLocalScaleX(-transform.localScale.x);

    }

    public void Jump()
    {
        onJump.OnNext(Unit.Default);

        RigidBody2d.velocity = new Vector2(RigidBody2d.velocity.x, 0);
        RigidBody2d.AddForce(new Vector2(0, JumpPower / 1.5f));
    }

    public void PlayShotSpark()
    {
        if (shotSparkCoroutine == null)
        {
            shotSparkCoroutine = StartCoroutine(ShotSpark());

            AudioManager.PlaySe(GameSE.PlayerShot);
        }
    }

    IEnumerator ShotSpark()
    {
        shotSparkTexture.transform.localScale = new Vector2(0.5f, 0.5f);
        shotSparkTexture.SetActive(true);

        yield return new WaitForSeconds(0.05f);

        shotSparkTexture.transform.localScale = new Vector2(0.8f, 0.8f);

        yield return new WaitForSeconds(0.05f);

        shotSparkTexture.SetActive(false);

        shotSparkCoroutine = null;
    }

    public void DamageByEnemyAttack(float damage)
    {
        if (IsInvinsible.Value)
        {
            return;
        }

        AudioManager.PlaySe(GameSE.PlayerDamage);

        MoveStateMachine.Change(PlayerMoveState.KnockBack);

        PlayerCharacterManager.Instance.Damage(damage);

        // ノックバック
        RigidBody2d.velocity = new Vector2(3.0f * (!FacingRight.Value ? 1 : -1), 2.0f);

        SetInvinsible(InvincibleTime);
    }

    public void SetInvinsible(int time = 0)
    {
        IsInvinsible.Value = true;

        if (time == 0)
        {
            InvinsibleTimer.Value = this.InvincibleTime;
        }
        else
        {
            InvinsibleTimer.Value = time;
        }
    }

    public void Dispose()
    {
        foreach(var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }

    public void SetAnimation(PlayerAnimationState animType)
    {
        if (!(waitTexture != null && walkTexture1 != null && walkTexture2 != null && walkTexture3 != null &&
            walkTexture4 != null && spaceJumpSprite != null && spaceJumpSprite != null && spaceJumpMotionBlur != null && damageSprite != null))
        {
            return;   
        }

        currentAnimeState = animType;

        switch(animType)
        {
            case PlayerAnimationState.Wait:
                StopWalkAnim();

                waitTexture.gameObject.SetActive(true);
                walkTexture1.gameObject.SetActive(false);
                walkTexture2.gameObject.SetActive(false);
                walkTexture3.gameObject.SetActive(false);
                walkTexture4.gameObject.SetActive(false);
                spaceJumpSprite.gameObject.SetActive(false);
                spaceJumpMotionBlur.SetActive(false);
                damageSprite.gameObject.SetActive(false);

                break;
            case PlayerAnimationState.Walk:
                if (walkAnimCoroutine == null)
                {
                    walkAnimCoroutine = StartCoroutine(WalkAnim());
                }
                break;
            case PlayerAnimationState.Jump:
            case PlayerAnimationState.Fall:
                StopWalkAnim();

                waitTexture.gameObject.SetActive(false);
                walkTexture1.gameObject.SetActive(true);
                walkTexture2.gameObject.SetActive(false);
                walkTexture3.gameObject.SetActive(false);
                walkTexture4.gameObject.SetActive(false);
                spaceJumpSprite.gameObject.SetActive(false);
                spaceJumpMotionBlur.SetActive(false);
                damageSprite.gameObject.SetActive(false);

                break;
            case PlayerAnimationState.KnockBack:
                StopWalkAnim();

                waitTexture.gameObject.SetActive(false);
                walkTexture1.gameObject.SetActive(false);
                walkTexture2.gameObject.SetActive(false);
                walkTexture3.gameObject.SetActive(false);
                walkTexture4.gameObject.SetActive(false);
                spaceJumpSprite.gameObject.SetActive(false);
                spaceJumpMotionBlur.SetActive(false);
                damageSprite.gameObject.SetActive(true);

                break;

            case PlayerAnimationState.Dead:
                StopWalkAnim();

                waitTexture.gameObject.SetActive(false);
                walkTexture1.gameObject.SetActive(false);
                walkTexture2.gameObject.SetActive(false);
                walkTexture3.gameObject.SetActive(false);
                walkTexture4.gameObject.SetActive(false);
                spaceJumpSprite.gameObject.SetActive(false);
                spaceJumpMotionBlur.SetActive(false);
                damageSprite.gameObject.SetActive(true);

                damageSprite.DOColor(Color.white, 1.0f);

                break;
            case PlayerAnimationState.SpaceJump:
                StopWalkAnim();

                waitTexture.gameObject.SetActive(false);
                walkTexture1.gameObject.SetActive(false);
                walkTexture2.gameObject.SetActive(false);
                walkTexture3.gameObject.SetActive(false);
                walkTexture4.gameObject.SetActive(false);
                spaceJumpSprite.gameObject.SetActive(true);
                spaceJumpMotionBlur.SetActive(true);
                damageSprite.gameObject.SetActive(false);

                break;
            case PlayerAnimationState.Finish:
                StopWalkAnim();

                waitTexture.gameObject.SetActive(false);
                walkTexture1.gameObject.SetActive(false);
                walkTexture2.gameObject.SetActive(false);
                walkTexture3.gameObject.SetActive(false);
                walkTexture4.gameObject.SetActive(false);
                spaceJumpSprite.gameObject.SetActive(false);
                spaceJumpMotionBlur.SetActive(false);
                damageSprite.gameObject.SetActive(false);
                finishSprite.gameObject.SetActive(true);

                break;
        }
    }

    IEnumerator WalkAnim()
    {
        while (true)
        {
            waitTexture.gameObject.SetActive(false);
            walkTexture1.gameObject.SetActive(true);
            walkTexture2.gameObject.SetActive(false);
            walkTexture3.gameObject.SetActive(false);
            walkTexture4.gameObject.SetActive(false);
            spaceJumpSprite.gameObject.SetActive(false);
            spaceJumpMotionBlur.SetActive(false);
            damageSprite.gameObject.SetActive(false);

            yield return new WaitForSeconds(0.1f);

            waitTexture.gameObject.SetActive(false);
            walkTexture1.gameObject.SetActive(false);
            walkTexture2.gameObject.SetActive(true);
            walkTexture3.gameObject.SetActive(false);
            walkTexture4.gameObject.SetActive(false);
            spaceJumpSprite.gameObject.SetActive(false);
            spaceJumpMotionBlur.SetActive(false);
            damageSprite.gameObject.SetActive(false);

            yield return new WaitForSeconds(0.1f);

            waitTexture.gameObject.SetActive(false);
            walkTexture1.gameObject.SetActive(false);
            walkTexture2.gameObject.SetActive(false);
            walkTexture3.gameObject.SetActive(true);
            walkTexture4.gameObject.SetActive(false);
            spaceJumpSprite.gameObject.SetActive(false);
            spaceJumpMotionBlur.SetActive(false);
            damageSprite.gameObject.SetActive(false);

            yield return new WaitForSeconds(0.1f);

            waitTexture.gameObject.SetActive(false);
            walkTexture1.gameObject.SetActive(false);
            walkTexture2.gameObject.SetActive(false);
            walkTexture3.gameObject.SetActive(false);
            walkTexture4.gameObject.SetActive(true);
            spaceJumpSprite.gameObject.SetActive(false);
            spaceJumpMotionBlur.SetActive(false);
            damageSprite.gameObject.SetActive(false);

            yield return new WaitForSeconds(0.1f);
        }
    }

    public void StopWalkAnim()
    {
        if (walkAnimCoroutine != null)
        {
            StopCoroutine(walkAnimCoroutine);
            walkAnimCoroutine = null;
        }
    }
}

