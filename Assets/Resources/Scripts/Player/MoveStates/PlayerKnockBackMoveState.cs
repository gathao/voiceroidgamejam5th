﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityConstants;

public class PlayerKnockBackMoveState : PlayerMoveStateBase
{
    int knockBackFrame = 40;
    int frameCounter = 0;

    public override void Setup(PlayerController player)
    {
        this.player = player;
    }

    public override void EnterState()
    {
        isDispose = false;
        frameCounter = 0;

        player.SetAnimation(PlayerAnimationState.KnockBack);

        player.UpdateAsObservable()
            .Where(_ => !PauseManager.Instance.Pausing)
            .Where(_ => !isDispose)
            .Where(_ => player.isPlayerble)
            .Subscribe(_ =>
            {
                if (frameCounter++ > knockBackFrame)
                {
                    if (!player.ExistsLandableGroundUnderFeet.Value)
                    {
                        player.SetAnimation(PlayerAnimationState.Jump);
                        player.MoveStateMachine.Change(PlayerMoveState.Air);
                    }
                    else
                    {
                        player.SetAnimation(PlayerAnimationState.Wait);
                        player.MoveStateMachine.Change(PlayerMoveState.Ground);
                    }
                }
            })
            .AddTo(disposables);
    }

    public override void ExitState()
    {
        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
        isDispose = true;
    }
}
