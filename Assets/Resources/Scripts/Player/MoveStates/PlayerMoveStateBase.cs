﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerMoveStateBase
{
    protected PlayerController player;

    protected List<IDisposable> disposables = new List<IDisposable>();

    protected bool isDispose = false;

    public abstract void Setup(PlayerController player);

    public abstract void EnterState();

    public abstract void ExitState();
}
