﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public enum PlayerMoveState
{
    Ground,
    Air,
    KnockBack,
    Dead,
}

public class PlayerMoveStateMachine
{
    Dictionary<PlayerMoveState, PlayerMoveStateBase> states;

    public PlayerMoveState currentState { get; protected set; }

    PlayerMoveStateBase currentMoveState;

    Subject<PlayerMoveState> onChanged = new Subject<PlayerMoveState>();

    public void SetUp(PlayerController player)
    {
        this.states = new Dictionary<PlayerMoveState, PlayerMoveStateBase>()
        {
            { PlayerMoveState.Ground, new PlayerGroundMoveState() },
            { PlayerMoveState.Air, new PlayerAirMoveState() },
            { PlayerMoveState.KnockBack, new PlayerKnockBackMoveState() },
            { PlayerMoveState.Dead, new PlayerDeadMoveState() },
        };

        foreach (var state in states.Values)
        {
            state.Setup(player);
        }
    }

    public void Change(PlayerMoveState nextState)
    {
        if (currentMoveState != null)
        {
            currentMoveState.ExitState();
        }

        currentState = nextState;
        currentMoveState = states[nextState];
        currentMoveState.EnterState();

        onChanged.OnNext(nextState);
    }


}
