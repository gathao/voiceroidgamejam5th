﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityConstants;

public class PlayerAirMoveState : PlayerMoveStateBase
{
    float accelerator = 0.0f;
    float acceleratorForce = 0.0f;

    float highJumpPower = 10.0f;
    float jumpDecay = 0.3f;

    bool highJumpComplete = false;

    public override void Setup(PlayerController player)
    {
        this.player = player;
        this.acceleratorForce = player.MoveSpeedX / 12.0f;
    }

    public override void EnterState()
    {
        this.isDispose = false;
        this.accelerator = 0.0f;
        this.highJumpPower = 10.0f;
        this.jumpDecay = 0.3f;
        this.highJumpComplete = false;

        player.SetAnimation(PlayerAnimationState.Jump);

        // ジャンプボタンを離すor一定時間立つと上昇をやめる
        player.FixedUpdateAsObservable()
            .Where(_ => !this.highJumpComplete)
            .Subscribe(_ => 
            {
                if (player.RigidBody2d.velocity.y <= 1.18f)
                {
                    player.RigidBody2d.velocity = new Vector2(player.RigidBody2d.velocity.x, 0.0f);
                    this.highJumpComplete = true;
                }
                else
                {
                    player.RigidBody2d.velocity = new Vector2(player.RigidBody2d.velocity.x, this.highJumpPower);
                    this.highJumpPower -= this.jumpDecay;
                }
            })
            .AddTo(disposables);

        // 空中ジャンプ防止
        player.UpdateAsObservable()
            .Where(_ => GameInput.Instance.GetButtonUp(GameKey.Jump))
            .Subscribe(_ => 
            {
                this.highJumpComplete = true;
            })
            .AddTo(disposables);

        // スペースジャンプ
        player.UpdateAsObservable()
            .Where(_ => this.player.isPlayerble)
            .Where(_ => GameInput.Instance.GetButtonDown(GameKey.Jump))
            .Where(_ => player.CanSpaceJump)
            .Where(_ => this.highJumpComplete)
            .Subscribe(_ =>
            {
                this.highJumpComplete = false;
                player.RigidBody2d.velocity = new Vector2(player.RigidBody2d.velocity.x, 0);

                float defaultJumpPower = player.JumpPower;
                player.JumpPower = 1000.0f;
                player.Jump();
                player.JumpPower = defaultJumpPower;

                player.SetAnimation(PlayerAnimationState.SpaceJump);

                AudioManager.PlaySe(GameSE.PlayerJump);
            })
            .AddTo(disposables);


        player.UpdateAsObservable()
            .Where(_ => !PauseManager.Instance.Pausing)
            .Where(_ => !isDispose)
            .Where(_ => player.isPlayerble)
            .Subscribe(_ =>
            {
                AirWalk();
            })
            .AddTo(disposables);

        // 着地したら地上状態にする
        player.ExistsLandableGroundUnderFeet
            .Where(val => val)
            .Where(_ => player.isPlayerble)
            .Subscribe(val =>
            {
                player.MoveStateMachine.Change(PlayerMoveState.Ground);

                if (player.RigidBody2d.velocity.x == 0.0f)
                {
                    player.SetAnimation(PlayerAnimationState.Wait);
                    player.RigidBody2d.velocity = new Vector2(0, player.RigidBody2d.velocity.y);
                }
                else
                {
                    player.SetAnimation(PlayerAnimationState.Walk);
                    player.RigidBody2d.velocity = new Vector2(0, player.RigidBody2d.velocity.y);
                }
            })
            .AddTo(disposables);
    }

    void AirWalk()
    {
        if (player.Walkable.Value && GameInput.Instance.GetRightArrowButton())
        {
            accelerator = Mathf.Min(player.MoveSpeedX, accelerator + acceleratorForce);

            player.RigidBody2d.velocity = new Vector2(accelerator, player.RigidBody2d.velocity.y);
            player.LookRight();
        }
        else if (player.Walkable.Value && GameInput.Instance.GetLeftArrowButton())
        {
            accelerator = Mathf.Max(-player.MoveSpeedX, accelerator - acceleratorForce);

            player.RigidBody2d.velocity = new Vector2(accelerator, player.RigidBody2d.velocity.y);
            player.LookLeft();
        }
    }

    public override void ExitState()
    {
        foreach (var item in disposables)
        {
            item.Dispose();
        }

        disposables.Clear();
        isDispose = true;
    }
}
