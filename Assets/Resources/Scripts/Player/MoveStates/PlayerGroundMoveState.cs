﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityConstants;

public class PlayerGroundMoveState : PlayerMoveStateBase
{
    bool isWalking = false;
    float accelerator = 0.0f;
    float acceleratorForce = 0.0f;

    Coroutine runCoroutine = null;

    public override void Setup(PlayerController player)
    {
        this.player = player;
        acceleratorForce = player.MoveSpeedX / 12.0f;
    }

    public override void EnterState()
    {
        this.isDispose = false;
        accelerator = 0.0f;

        player.UpdateAsObservable()
            .Where(_ => !PauseManager.Instance.Pausing)
            .Where(_ => !isDispose)
            .Where(_ => !PlayerCharacterManager.Instance.Deaded.Value)
            .Where(_ => this.player.isPlayerble)
            .Subscribe(_ =>
            {
                Walk();

                if (GameInput.Instance.GetButtonDown(GameKey.Jump))
                {
                    player.Jump();
                    AudioManager.PlaySe(GameSE.PlayerJump);
                }
            })
            .AddTo(disposables);

        // 地上から離れたら空中判定にする
        player.ExistsLandableGroundUnderFeet
            .Where(val => !val)
            .Subscribe(_ => 
            {
                if (!player.ExistsLandableGroundUnderFeet.Value)
                {
                    isWalking = false;
                    player.MoveStateMachine.Change(PlayerMoveState.Air);
                }
            })
            .AddTo(disposables);    

        player.FixedUpdateAsObservable()
            .Where(_ => !isDispose)
            .Where(_ => !isWalking)
            .Where(_ => player.isPlayerble)
            .Subscribe(_ =>
            {
                player.SetAnimation(PlayerAnimationState.Wait);
                player.RigidBody2d.velocity = new Vector2(0, player.RigidBody2d.velocity.y);
            })
            .AddTo(disposables);

        player.FixedUpdateAsObservable()
            .Where(_ => !player.isPlayerble)
            .Subscribe(_ =>
            {
                player.RigidBody2d.velocity = Vector2.zero;
            });
    }

    void Walk()
    {
        if (player.Walkable.Value && GameInput.Instance.GetRightArrowButton())
        {
            accelerator = Mathf.Min(player.MoveSpeedX, accelerator + acceleratorForce);

            player.RigidBody2d.velocity = new Vector2(accelerator, player.RigidBody2d.velocity.y);
            player.SetAnimation(PlayerAnimationState.Walk);
            player.LookRight();
            isWalking = true;

            if (runCoroutine == null)
            {
                runCoroutine = player.StartCoroutine(RunSE());
            }
        }
        else if (player.Walkable.Value && GameInput.Instance.GetLeftArrowButton())
        {
            accelerator = Mathf.Max(-player.MoveSpeedX, accelerator - acceleratorForce);

            player.RigidBody2d.velocity = new Vector2(accelerator, player.RigidBody2d.velocity.y);
            player.SetAnimation(PlayerAnimationState.Walk);
            player.LookLeft();
            isWalking = true;

            if (runCoroutine == null)
            {
                runCoroutine = player.StartCoroutine(RunSE());
            }

        }
        else
        {
            accelerator = 0.0f;
            isWalking = false;

            StopRunSE();
        }
    }

    IEnumerator RunSE()
    {
        while (true)
        {
            AudioManager.PlaySe(GameSE.PlayerWalk);

            yield return new WaitForSeconds(0.3f);
        }
    }

    void StopRunSE()
    {
        if (runCoroutine != null)
        {
            player.StopCoroutine(runCoroutine);
            runCoroutine = null;
        }
    }

    public override void ExitState()
    {
        StopRunSE();

        foreach (var item in disposables)
        {
            item.Dispose();
        }

        disposables.Clear();
        isDispose = true;
    }
}
