﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityConstants;

public class PlayerDeadMoveState : PlayerMoveStateBase
{
    public override void Setup(PlayerController player)
    {
        this.player = player;
    }

    public override void EnterState()
    {
        isDispose = false;

        Debug.Log("プレイヤー死亡");

        // 当たり判定を消去するため、SetInvinsibleメソッドを使う。
        // 死んだのに無敵とは矛盾しているが、処理が似通っているからゆるして
        // NOTE: 無敵フラグ付与によって変な問題発生したら別メソッド作る
        player.SetInvinsible(int.MaxValue);

        player.ExistsLandableGroundUnderFeet
            .Where(x => x)
            .Subscribe(_ =>
            {
                //AudioManager.PlaySe(GameSE.SuzuDead);
                player.SetAnimation(PlayerAnimationState.Dead);
                player.RigidBody2d.velocity = Vector2.zero;
            })
            .AddTo(disposables);
    }

    public override void ExitState()
    {
        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
        isDispose = true;
    }
}
