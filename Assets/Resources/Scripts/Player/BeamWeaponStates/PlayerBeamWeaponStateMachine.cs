﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public enum PlayerBeamWeaponState
{
    NormalBeam, // 通常 威力:1
    PlasmaBeam, // 敵貫通・広範囲 威力:2
    WaveBeam,   // 敵貫通・壁貫通・広範囲 威力:4
}

public class PlayerBeamWeaponStateMachine
{
    // ビームの一覧
    Dictionary<PlayerBeamWeaponState, PlayerBeamWeaponStateBase> states;

    public PlayerBeamWeaponStateBase currentBeamWeapon;

    public PlayerBeamWeaponState WeaponState { get; private set; }

    Subject<PlayerBeamWeaponState> onChanged = new Subject<PlayerBeamWeaponState>();

    public void SetUp(PlayerController player)
    {
        states = new Dictionary<PlayerBeamWeaponState, PlayerBeamWeaponStateBase>()
        {
            { PlayerBeamWeaponState.NormalBeam, new PlayerNormalBeamWeaponState() },
            { PlayerBeamWeaponState.PlasmaBeam, new PlayerPlasmaBeamWeaponState() },
            { PlayerBeamWeaponState.WaveBeam, new PlayerWaveBeamWeaponState() },
        };

        foreach (var weapon in states.Values)
        {
            weapon.Setup(player);
        }
    }

    public void Change(PlayerBeamWeaponState nextState)
    {
        if (currentBeamWeapon != null)
        {
            currentBeamWeapon.TakeOff();
        }

        WeaponState = nextState;
        currentBeamWeapon = states[nextState];
        currentBeamWeapon.Equipment();
        onChanged.OnNext(nextState);
    }
}
