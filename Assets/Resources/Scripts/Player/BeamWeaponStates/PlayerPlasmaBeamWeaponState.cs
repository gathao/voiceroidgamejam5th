﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using DG.Tweening;

public class PlayerPlasmaBeamWeaponState : PlayerBeamWeaponStateBase
{
    PlasmaBeamContainer plasmaBeamContainer = null;

    readonly Vector3 ShotOffset = new Vector3(0.95f, 0.95f, 0.0f);

    public override void Setup(PlayerController player)
    {
        this.player = player;
    }

    public override void Equipment()
    {
        float shotAngle = 0.0f;
        float radian = 0.0f;

        player
            .UpdateAsObservable()
            .Where(_ => !PauseManager.Instance.Pausing)
            .Where(_ => !PlayerCharacterManager.Instance.Deaded.Value)
            .Where(_ => this.player.isPlayerble)
            .Where(_ => GameInput.Instance.GetButtonDown(GameKey.Action1))
            .Subscribe(_ =>
            {
                // 発射角度
                var h = Input.GetAxis("Horizontal");
                var v = Input.GetAxis("Vertical");

                // 入力がない間は最後の角度のままにする
                if (!(h == 0.0f && v == 0.0f))
                {
                    radian = Mathf.Atan2(v, h);
                }

                if (radian > 0.0f)
                {
                    radian = radian + Mathf.PI * 2.0f;
                }

                shotAngle = radian * Mathf.Rad2Deg;

                // 左向きの場合、オフセットを反転
                var dir = player.FacingRight.Value ? 1.0f : -1.0f;
                var shotOffset = new Vector3(ShotOffset.x * dir, ShotOffset.y, ShotOffset.z);

                // プラズマビーム発射
                // 3発、直線
                player.PlayShotSpark();

                var prefab = PlayerBeamsMasterData.Instance[BeamWeaponType.PlasmaBeam];

                var container = GameObject.Instantiate(prefab).GetComponent<PlasmaBeamContainer>();
                container.SetUp(player.transform.position + shotOffset, shotAngle, 0.0f, WeaponOwner.Player);

                var plasmaBeamTop = container.plasmaBeamTop;
                plasmaBeamTop.SetUp(plasmaBeamTop.transform.localPosition, shotAngle, 22.0f, WeaponOwner.Player);
                plasmaBeamTop.WeaponStart();

                var plasmaBeamCenter = container.plasmaBeamCenter;
                plasmaBeamCenter.SetUp(plasmaBeamCenter.transform.localPosition, shotAngle, 22.0f, WeaponOwner.Player);
                plasmaBeamCenter.WeaponStart();

                var plasmaBeamBottom = container.plasmaBeamBottom;
                plasmaBeamBottom.SetUp(plasmaBeamBottom.transform.localPosition, shotAngle, 22.0f, WeaponOwner.Player);
                plasmaBeamBottom.WeaponStart();

                container.WeaponStart();
            })
            .AddTo(disposables);
    }


    public override void TakeOff()
    {
        // 外した瞬間に消すならStopする
        //if (normalBeam != null)
        //{
        //    normalBeam.WeaponStop();
        //}

        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }
}
