﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class PlayerBeamWeaponStateBase
{
    protected PlayerController player;

    protected float shotAngle = 0.0f;

    protected List<IDisposable> disposables = new List<IDisposable>();

    protected bool isDispose = false;

    public abstract void Setup(PlayerController player);

    public abstract void Equipment();

    public abstract void TakeOff();
}
