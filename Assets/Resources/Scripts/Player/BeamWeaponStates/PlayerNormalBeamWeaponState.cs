﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using DG.Tweening;

public class PlayerNormalBeamWeaponState : PlayerBeamWeaponStateBase
{
    PlayerNormalBeam normalBeam = null;

    Vector3 shotOffset = new Vector3(0.45f, 0.95f, 0.0f);

    public override void Setup(PlayerController player)
    {
        this.player = player;
    }

    public override void Equipment()
    {
        float shotAngle = 0.0f;
        float radian = 0.0f;

        player
            .UpdateAsObservable()
            .Where(_ => !PauseManager.Instance.Pausing)
            .Where(_ => !PlayerCharacterManager.Instance.Deaded.Value)
            .Where(_ => this.player.isPlayerble)
            .Where(_ => GameInput.Instance.GetButtonDown(GameKey.Action1))
            .Subscribe(_ =>
            {
                // 発射角度
                var h = Input.GetAxis("Horizontal");
                var v =Input.GetAxis("Vertical");

                // 入力がない間は最後の角度のままにする
                if (!(h == 0.0f && v == 0.0f))
                {
                    radian = Mathf.Atan2(v, h);
                }

                if (radian > 0.0f)
                {
                    radian = radian + Mathf.PI * 2.0f;
                }

                shotAngle = radian * Mathf.Rad2Deg;

                // 通常ビーム発射
                // 1発、直線
                player.PlayShotSpark();

                var prefab = PlayerBeamsMasterData.Instance[BeamWeaponType.NormalBeam];

                normalBeam = GameObject.Instantiate(prefab).GetComponent<PlayerNormalBeam>();
                normalBeam.SetUp(player.transform.position + shotOffset, shotAngle, 15.0f, WeaponOwner.Player);
                normalBeam.WeaponStart();
            })
            .AddTo(disposables);
    }


    public override void TakeOff()
    {
        // 外した瞬間に消すならStopする
        //if (normalBeam != null)
        //{
        //    normalBeam.WeaponStop();
        //}

        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }
}

