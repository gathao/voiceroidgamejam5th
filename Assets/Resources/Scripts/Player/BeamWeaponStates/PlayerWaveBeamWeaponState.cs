﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using DG.Tweening;

public class PlayerWaveBeamWeaponState : PlayerBeamWeaponStateBase
{
    WaveBeamContainer waveBeamContainer = null;

    readonly Vector3 ShotOffset = new Vector3(0.55f, 0.95f, 0.0f);

    public override void Setup(PlayerController player)
    {
        this.player = player;
    }

    public override void Equipment()
    {
        float shotAngle = 0.0f;
        float radian = 0.0f;

        player
            .UpdateAsObservable()
            .Where(_ => !PauseManager.Instance.Pausing)
            .Where(_ => !PlayerCharacterManager.Instance.Deaded.Value)
            .Where(_ => this.player.isPlayerble)
            .Where(_ => GameInput.Instance.GetButtonDown(GameKey.Action1))
            .Subscribe(_ =>
            {
                // 発射角度
                var h = Input.GetAxis("Horizontal");
                var v = Input.GetAxis("Vertical");

                // 入力がない間は最後の角度のままにする
                if (!(h == 0.0f && v == 0.0f))
                {
                    radian = Mathf.Atan2(v, h);
                }

                if (radian > 0.0f)
                {
                    radian = radian + Mathf.PI * 2.0f;
                }

                shotAngle = radian * Mathf.Rad2Deg;

                // 左向きの場合、オフセットを反転
                var dir = player.FacingRight.Value ? 1.0f : -1.0f;
                var shotOffset = new Vector3(ShotOffset.x * dir, ShotOffset.y, ShotOffset.z);

                // ウェーブビーム発射
                // 直線1発、Sinカーブ2発
                player.PlayShotSpark();

                var prefab = PlayerBeamsMasterData.Instance[BeamWeaponType.WaveBeam];

                var container = GameObject.Instantiate(prefab).GetComponent<WaveBeamContainer>();
                container.SetUp(player.transform.position + shotOffset, shotAngle, 0.0f, WeaponOwner.Player);

                var waveBeamTop = container.waveBeamTop;
                waveBeamTop.SetUp(waveBeamTop.transform.localPosition, shotAngle, 20.0f, WeaponOwner.Player);
                waveBeamTop.WeaponStart();

                var waveBeamCenter = container.waveBeamCenter;
                waveBeamCenter.SetUp(waveBeamCenter.transform.localPosition, shotAngle, 20.0f, WeaponOwner.Player);
                waveBeamCenter.WeaponStart();

                var waveBeamBottom = container.waveBeamBottom;
                waveBeamBottom.SetUp(waveBeamBottom.transform.localPosition, shotAngle, 20.0f, WeaponOwner.Player);
                waveBeamBottom.WeaponStart();

                container.WeaponStart();
            })
            .AddTo(disposables);
    }


    public override void TakeOff()
    {
        // 外した瞬間に消すならStopする
        //if (normalBeam != null)
        //{
        //    normalBeam.WeaponStop();
        //}

        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }
}
