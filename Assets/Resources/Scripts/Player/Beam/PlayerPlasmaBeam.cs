﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniRx.Triggers;
using UniRx;
using UnityConstants;
using System;

public class PlayerPlasmaBeam : BeamBase
{
    [SerializeField]
    SpriteRenderer beamSprite = null;

    [SerializeField]
    SpriteRenderer fireSprite01 = null;

    [SerializeField]
    SpriteRenderer fireSprite02 = null;

    Coroutine fireCoroutine = null;

    public override void SetUp(Vector3 position, float angle, float speed, WeaponOwner weaponOwner)
    {
        base.SetUp(position, angle, speed, weaponOwner);

        Damage = 1.2f;
    }

    public override void WeaponStart()
    {
        beamSprite.gameObject.SetActive(true);
        fireSprite01.gameObject.SetActive(false);
        fireSprite02.gameObject.SetActive(false);

        attackCollider.gameObject.SetActive(true);

        // 弾の向きと飛んでいくベクトル
        rigidbody2D.rotation = angle;
        rigidbody2D.velocity = Quaternion.Euler(0, 0, angle) * new Vector2(speed, 0.0f);

        // 壁に当たった時の処理
        attackCollider
            .OnTriggerEnter2DAsObservable()
            .Where(col => col.gameObject.layer == Layers.Ground)
            .Subscribe(_ =>
            {
                isGroundHited = true;
                WeaponHit();
            })
            .AddTo(disposables);

        // 敵に当たった時の処理
        attackCollider
            .OnTriggerEnter2DAsObservable()
            .Where(col => col.gameObject.layer == Layers.EnemyKuraiHitbox)
            .Subscribe(obj =>
            {
                // 敵に当たった時は消えない（貫通）
                //WeaponHit();

                Camera.main.GetComponent<GameCameraController>().Shake();

                // ダメージを与える
                var enemy = obj.GetComponentInParent<EnemyBase>();

                // EffectManager
                enemy.Damage(Damage);
            });
    }

    public override void WeaponHit()
    {
        rigidbody2D.velocity = Vector2.zero;

        attackCollider.gameObject.SetActive(false);
        fireCoroutine = StartCoroutine(HitSpark());

        this.FixedUpdateAsObservable()
            .Where(_ => fireCoroutine == null)
            .Subscribe(_ =>
            {
                Dispose();
                Destroy(gameObject);
            })
            .AddTo(disposables);
    }

    IEnumerator HitSpark()
    {
        beamSprite.gameObject.SetActive(false);
        fireSprite01.gameObject.SetActive(true);
        fireSprite02.gameObject.SetActive(false);

        yield return new WaitForSeconds(0.1f);

        beamSprite.gameObject.SetActive(false);
        fireSprite01.gameObject.SetActive(false);
        fireSprite02.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.1f);

        beamSprite.gameObject.SetActive(false);
        fireSprite01.gameObject.SetActive(false);
        fireSprite02.gameObject.SetActive(false);

        fireCoroutine = null;
    }
}
