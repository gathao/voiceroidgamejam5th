﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniRx.Triggers;
using UniRx;
using UnityConstants;
using System;

public class WaveBeamContainer : BeamBase
{
    [SerializeField]
    public PlayerWaveBeam waveBeamTop = null;

    [SerializeField]
    public PlayerWaveBeam waveBeamCenter = null;

    [SerializeField]
    public PlayerWaveBeam waveBeamBottom = null;

    const float beamDefutionSec = 0.1f;

    float shotTime = 0.0f;
    float waveDeg = 1.0f;

    public override void SetUp(Vector3 position, float angle, float speed, WeaponOwner weaponOwner)
    {
        base.SetUp(position, angle, speed, weaponOwner);
    }

    public override void WeaponStart()
    {
        var dir = PlayerCharacterManager.Instance.Player.FacingRight.Value ? 1.0f : -1.0f;

        Observable
            .EveryFixedUpdate()
            .Subscribe(_ =>
            {
                shotTime += 0.1f;

                if (waveBeamTop != null)
                {
                    var x = Mathf.Sin(shotTime * 2.0f) * 0.4f;
                    var y = Mathf.Cos(shotTime * 2.0f) * 0.4f;

                    waveBeamTop.transform.localPosition = new Vector2(
                    waveBeamTop.transform.localPosition.x,
                    waveBeamTop.transform.localPosition.y + y);

                    var radian = Math.Abs(Mathf.Atan2(y, x)) + (Mathf.PI * 1.5f);

                    waveBeamTop.Rigidbody2D.rotation = radian * Mathf.Rad2Deg;
                }

                if (waveBeamBottom != null)
                {
                    var x = Mathf.Sin(shotTime * 2.0f) * 0.4f;
                    var y = Mathf.Cos(shotTime * 2.0f) * -0.4f;

                    waveBeamBottom.transform.localPosition = new Vector2(
                    waveBeamBottom.transform.localPosition.x,
                    waveBeamBottom.transform.localPosition.y + y);

                    var radian = Math.Abs(Mathf.Atan2(y, x)) + (Mathf.PI * 1.5f);

                    waveBeamBottom.Rigidbody2D.rotation = -radian * Mathf.Rad2Deg;
                }
            })
            .AddTo(this);


        // ビームがすべて消えたら自分も破棄する
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                if (waveBeamTop == null && waveBeamCenter == null && waveBeamBottom == null)
                {
                    Destroy(gameObject);
                }
            })
            .AddTo(this);
    }
}
