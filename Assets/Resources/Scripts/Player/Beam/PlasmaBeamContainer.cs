﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniRx.Triggers;
using UniRx;
using UnityConstants;
using System;


public class PlasmaBeamContainer : BeamBase
{
    [SerializeField]
    public PlayerPlasmaBeam plasmaBeamTop = null;

    [SerializeField]
    public PlayerPlasmaBeam plasmaBeamCenter = null;

    [SerializeField]
    public PlayerPlasmaBeam plasmaBeamBottom = null;

    const float beamDefutionSec = 0.1f;

    public override void SetUp(Vector3 position, float angle, float speed, WeaponOwner weaponOwner)
    {
        base.SetUp(position, angle, speed, weaponOwner);
    }

    public override void WeaponStart()
    {
        var dir = PlayerCharacterManager.Instance.Player.FacingRight.Value ? 1.0f : -1.0f;

        var rotTopStart = Quaternion.Euler(0, 0, angle) * new Vector2(plasmaBeamTop.transform.localPosition.x * dir, 0.0f);
        var rotTop = Quaternion.Euler(0, 0, angle) * new Vector2(plasmaBeamTop.transform.localPosition.x * dir, plasmaBeamTop.transform.localPosition.y);

        var rotBottomStart = Quaternion.Euler(0, 0, angle) * new Vector2(plasmaBeamBottom.transform.localPosition.x * dir, 0.0f);
        var rotBottom = Quaternion.Euler(0, 0, angle) * new Vector2(plasmaBeamBottom.transform.localPosition.x * dir, plasmaBeamBottom.transform.localPosition.y);

        plasmaBeamTop.transform.localPosition = rotTopStart;
        plasmaBeamBottom.transform.localPosition = rotBottomStart;

        // 3本のビームが広がる演出
        plasmaBeamCenter.transform.DOLocalMove(Vector2.zero, beamDefutionSec);
        plasmaBeamTop.transform.DOLocalMove(rotTop, beamDefutionSec);
        plasmaBeamBottom.transform.DOLocalMove(rotBottom, beamDefutionSec);

        // ビームがすべて消えたら自分も破棄する
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                if (plasmaBeamCenter == null && plasmaBeamTop == null && plasmaBeamBottom == null)
                {
                    Destroy(gameObject);
                }
            })
            .AddTo(this);
    }
}
