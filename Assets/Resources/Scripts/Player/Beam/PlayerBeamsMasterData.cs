﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BeamWeaponType
{
    NormalBeam,
    PlasmaBeam,
    WaveBeam,
}

public class PlayerBeamsMasterData : SingletonMonoBehaviour<PlayerBeamsMasterData>
{
    [SerializeField]
    BeamBase NormalBeam = null;

    [SerializeField]
    BeamBase PlasmaBeam = null;

    [SerializeField]
    BeamBase WaveBeam = null;

    Dictionary<BeamWeaponType, BeamBase> BeamWeaponList;

    public void Start()
    {
        BeamWeaponList = new Dictionary<BeamWeaponType, BeamBase>()
        {
            { BeamWeaponType.NormalBeam, NormalBeam },
            { BeamWeaponType.PlasmaBeam, PlasmaBeam },
            { BeamWeaponType.WaveBeam, WaveBeam },
        };
    }

    public BeamBase this[BeamWeaponType type]
    {
        get
        {
            BeamBase result;
            if (BeamWeaponList.TryGetValue(type, out result))
            {
                return result;
            }

            return null;
        }
    }
}
