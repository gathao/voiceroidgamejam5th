﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityConstants;
using UnityEngine;

public enum WeaponOwner
{
    Player,
    Enemy,
}

public class BeamBase : MonoBehaviour
{
    [SerializeField]
    protected Collider2D attackCollider = null;

    [SerializeField]
    protected Rigidbody2D rigidbody2D = null;

    public Rigidbody2D Rigidbody2D
    {
        get
        {
            return this.rigidbody2D;
        }
    }

    [SerializeField]
    protected ParticleSystem particle = null;

    protected float Damage = 1.0f;

    protected float angle = 0.0f;

    protected float speed = 0.5f;

    protected WeaponOwner weaponOwner;

    protected BeamWeaponType beamWeaponType = BeamWeaponType.NormalBeam;

    protected List<IDisposable> disposables = new List<IDisposable>();

    protected bool isGroundHited = false;

    public virtual void SetUp(Vector3 position, float angle, float speed, WeaponOwner weaponOwner)
    {
        transform.localPosition = position;
        this.angle = angle;
        this.speed = speed;
        this.weaponOwner = weaponOwner;
        
        if (attackCollider != null)
        {
            // ダメージを与える処理
            switch (weaponOwner)
            {
                case WeaponOwner.Player:
                    attackCollider.gameObject.layer = Layers.PlayerAttack;
                    break;
                case WeaponOwner.Enemy:
                    attackCollider.gameObject.layer = Layers.EnemyAttack;

                    attackCollider
                        .OnTriggerEnter2DAsObservable()
                        .Where(col => col.gameObject.layer == Layers.PlayerKuraiHitbox)
                        .Where(_ => !PlayerCharacterManager.Instance.Player.IsInvinsible.Value)
                        .Subscribe(_ =>
                        {
                            PlayerCharacterManager.Instance.Player.DamageByEnemyAttack(this.Damage);
                        })
                        .AddTo(disposables);

                    break;
                default:
                    break;
            }

            // カメラの範囲外に出たら消す処理
            attackCollider
                .OnTriggerExit2DAsObservable()
                .Where(col => col.gameObject.layer == Layers.CameraBoundingBox)
                .Where(_ => !isGroundHited)
                .Subscribe(_ =>
                {
                    Dispose();
                    Destroy(gameObject);
                })
                .AddTo(disposables);

            attackCollider.gameObject.SetActive(false);
        }
    }

    public void SetDamage(float damage)
    {
        this.Damage = damage;
    }

    public virtual void WeaponStart()
    {

    }

    public virtual void WeaponHit()
    {

    }

    public void Dispose()
    {
        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }
}
