﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using UniRx.Triggers;

public class PlayerCharacterManager : SingletonMonoBehaviour<PlayerCharacterManager>
{
    [SerializeField]
    PlayerController player = null;

    public PlayerController Player
    {
        get { return player; }
    }

    public const int MaxHP = 100;

    public FloatReactiveProperty Hp { get; private set; }
    public ReadOnlyReactiveProperty<bool> Deaded { get; private set; }


    // プレイヤーが持っているカギアイテムの個数
    public IntReactiveProperty HasKagi { get; set; } = new IntReactiveProperty(0);

    List<IDisposable> disposables = new List<IDisposable>();

    // プレイヤーが一度でもダメージを受けたフラグ
    public bool Damaged { get; private set; } = false;

    /// <summary>
    /// ダメージ受けたときの処理
    /// </summary>
    /// <param name="value"></param>
    public void Damage(float value)
    {
        Damaged = true;

        if (Hp.Value - value < 0)
        {
            Hp.Value = 0;
        }
        else
        {
            Hp.Value -= value;
        }
    }

    /// <summary>
    /// プレイヤーが回復する
    /// </summary>
    /// <param name="value"></param>
    public void LifeUp(int value)
    {
        if (Hp.Value + value > MaxHP)
        {
            Hp.Value = MaxHP;
        }
        else
        {
            Hp.Value += value;
        }
    }

    protected override void Awake()
    {
        base.Awake();

        Hp = new FloatReactiveProperty(MaxHP);
        Deaded = Hp.Select(hp => hp <= 0).ToReadOnlyReactiveProperty();

        Deaded
            .Where(v => v)
            .Take(1)
            .DelayFrame(5)
            .Do(_ => player.MoveStateMachine.Change(PlayerMoveState.Dead))
            .Delay(TimeSpan.FromSeconds(1))
            .Subscribe(_ =>
            {
                InGameManager.Instance.PlayerDeaded();
            }).AddTo(disposables);
    }

    // 死亡状態ではない＆ジャンプ可能状態（＝地上にいる）ときイベント実行可能
    public bool IsEventStartPossible()
    {
        return !Deaded.Value && player.ExistsLandableGroundUnderFeet.Value;
    }

    // すべてのObserverを破棄
    public void Dispose()
    {
        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();

        player.Dispose();
    }

}
