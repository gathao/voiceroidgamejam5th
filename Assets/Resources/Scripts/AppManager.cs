﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using UniRx;
using UniRx.Triggers;
using DG.Tweening;
using UnityConstants;

public enum SceneState
{
    Title,
    Map,
    Game,
    Demo,
}

public class AppManager : SingletonMonoBehaviour<AppManager>
{
    public SceneState CurrentSceneState = SceneState.Title;

    public string CurrentStageName = SceneNames.TitleScene;

    // 死亡時のリスタート地点
    public bool StageRestart = false;
    public Vector3 PlayerReSpawnPoint;
    public string PlayerReStartAreaName = "";

    Subject<Unit> onSceneLoaded = new Subject<Unit>();
    public IObservable<Unit> OnSceneLoaded { get { return onSceneLoaded; } }

    // ゲームの進捗度データのファイルパス
    public string SavePath { get; private set; }

    // オプション設定データのファイルパス
    public string ConfigDataPath { get; private set; }

    // ゲーム画面の大きさを定義
    // [RuntimeInitializeOnLoadMethod]がついたメソッドはAwakeより先に呼び出される
    // さらにヒエラルキーに配置してなくても勝手に生成される(らしい)
    // http://tsubakit1.hateblo.jp/entry/2016/07/29/073000　詳しくはこのへん参照

    public static readonly Vector2 WindowResolution = new Vector2(1024, 640);
    [RuntimeInitializeOnLoadMethod]
    static void OnRuntimeMethodLoad()
    {
        Screen.SetResolution((int)WindowResolution.x, (int)WindowResolution.y, false, 60);
    }

    protected override void Awake()
    {
        base.Awake();
        DOTween.Init();

        // マウスカーソルを非表示に
        Cursor.visible = false;

        // セーブデータの保存先フォルダを最初に指定
        // Application.persistentDataPath はAwakeのタイミングでしか呼べないらしい
        SavePath = Application.dataPath + "/save.dat";
        ConfigDataPath = Application.dataPath + "/config.dat";

        this.UpdateAsObservable()
            .Take(1)
            .Subscribe(_ =>
            {
                onSceneLoaded.OnNext(Unit.Default);
            });

        //SaveDataManager.Instance.TryLoad();
        //SaveDataManager.Instance.TryConfigLoad();
    }

    void Start()
    {
        // 現在読み込んでいるシーンの一覧を取得
        var scenes = Enumerable.Range(0, SceneManager.sceneCount).Select(idx => SceneManager.GetSceneAt(idx));

        // 読み込みシーンが1つのみ＝AppBaseSceneしか読み込んでいない場合＝EXE起動した場合
        if (scenes.Count(s => s.isLoaded) == 1)
        {
            // タイトルシーンに飛ばす
            SceneManager
                .LoadSceneAsync(SceneNames.TitleScene, LoadSceneMode.Additive)
                .AsObservable()
                .Where(op => op.isDone)
                .Subscribe(__ =>
                {
                    CurrentStageName = SceneNames.TitleScene;
                    CurrentSceneState = SceneState.Title;
                    Debug.Log("タイトルシーン読み込み完了");
                });
        }
    }

    IObservable<AsyncOperation> UnloadScene(string sceneName)
    {
        return SceneManager.UnloadSceneAsync(sceneName).AsObservable();
    }
    IObservable<AsyncOperation> LoadScene(string sceneName)
    {
        return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive).AsObservable();
    }

    public void ChangeToTitleScene()
    {
        FadeInOutManager.Instance.FadeOut().Subscribe(_ =>
        {
            var oldSceneState = CurrentSceneState;
            CurrentSceneState = SceneState.Title;

            if (oldSceneState == SceneState.Game)
            {
                //SaveDataManager.Instance.TryLoad();

                SceneManager
                    .UnloadSceneAsync(CurrentStageName)
                    .AsObservable()
                    .SelectMany(UnloadScene(SceneNames.InGameScene))
                    .SelectMany(LoadScene(SceneNames.TitleScene))
                    .Subscribe(__ =>
                    {
                        var scene = SceneManager.GetSceneByName(SceneNames.TitleScene);
                        onSceneLoaded.OnNext(Unit.Default);

                        CurrentStageName = SceneNames.TitleScene;
                        CurrentSceneState = SceneState.Title;

                        FadeInOutManager.Instance.FadeIn().Subscribe(___ =>
                        {
                            SceneManager.SetActiveScene(scene);
                        });
                    });
            }
            else
            {
                Debug.LogError("ゲームシーン以外からTitleSceneが読み込まれている");
            }
        });
    }

    public void ChangeToInGameScene(string stageName)
    {
        FadeInOutManager.Instance.FadeOut().Subscribe(_ =>
        {
            SceneManager
            .UnloadSceneAsync(CurrentStageName)
            .AsObservable()
            //.SelectMany(UnloadScene(SceneNames.GameBaseScene))
            .SelectMany(LoadScene(SceneNames.InGameScene))
            .SelectMany(LoadScene(stageName))
            .Subscribe(__ =>
            {
                var scene = SceneManager.GetSceneByName(stageName);
                SceneManager.SetActiveScene(scene);
                InGameManager.Instance.StartGameScene();
                

                CurrentStageName = stageName;
                CurrentSceneState = SceneState.Game;

                FadeInOutManager.Instance.FadeIn().Subscribe(___ =>
                {
                    SceneManager.SetActiveScene(scene);
                });
            });

        });
    }

    // ゲーム画面でプレイヤー死亡時にリスタートするとき読み込まれる
    public void GameSceneReload()
    {
        FadeInOutManager.Instance.FadeOut().Subscribe(_ =>
        {
            SceneManager
            .UnloadSceneAsync(CurrentStageName)
            .AsObservable()
            .SelectMany(UnloadScene(SceneNames.InGameScene))
            .SelectMany(LoadScene(SceneNames.InGameScene))
            .SelectMany(LoadScene(CurrentStageName))
            .Subscribe(__ =>
            {
                var scene = SceneManager.GetSceneByName(CurrentStageName);
                InGameManager.Instance.StartGameScene();

                CurrentSceneState = SceneState.Game;

                FadeInOutManager.Instance.FadeIn().Subscribe(___ =>
                {
                    SceneManager.SetActiveScene(scene);
                });
            });
        });
    }
}
