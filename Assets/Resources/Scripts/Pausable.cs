﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class Pausable : MonoBehaviour
{
    List<BasePauser> pausers = new List<BasePauser>();

    void Awake()
    {
        pausers.Add(new Rigidbody2dPauser(this));
        pausers.Add(new MonoBehaviourPauser(this));
        pausers.Add(new AnimatorPauser(this));
    }

    public bool Pausing { get; private set; }

    public GameObject[] ignoreGameObjects;

    public void Pause()
    {
        Debug.Assert(!Pausing);
        Pausing = true;
        pausers.ForEach(p => p.Pause());
    }

    public void Resume()
    {
        Debug.Assert(Pausing);
        Pausing = false;
        pausers.ForEach(p => p.Resume());
    }

    #region pausers

    abstract class BasePauser
    {
        protected Pausable parent;
        public BasePauser(Pausable parent)
        {
            this.parent = parent;
        }
        public abstract void Pause();
        public abstract void Resume();
    }

    class Rigidbody2dPauser : BasePauser
    {
        RigidbodyVelocity[] rigidbodyVelocities;
        Rigidbody2D[] pausingRigidbodies;

        class RigidbodyVelocity
        {
            public Vector3 velocity;
            public float angularVeloccity;
            public float gravityScale;

            public RigidbodyVelocity(Rigidbody2D rigidbody)
            {
                velocity = rigidbody.velocity;
                angularVeloccity = rigidbody.angularVelocity;
                gravityScale = rigidbody.gravityScale;
            }
        }

        public Rigidbody2dPauser(Pausable parent) : base(parent) { }

        public override void Pause()
        {
            // Rigidbody2Dの停止
            // 子要素から、スリープ中でなく、IgnoreGameObjectsに含まれていないRigidbodyを抽出
            Predicate<Rigidbody2D> rigidbodyPredicate =
                obj => !obj.IsSleeping() &&
                       Array.FindIndex(parent.ignoreGameObjects, gameObject => gameObject == obj.gameObject) < 0;
            pausingRigidbodies = Array.FindAll(parent.transform.GetComponentsInChildren<Rigidbody2D>(), rigidbodyPredicate);
            rigidbodyVelocities = new RigidbodyVelocity[pausingRigidbodies.Length];
            for (int i = 0; i < pausingRigidbodies.Length; i++)
            {
                // 速度、角速度も保存しておく
                rigidbodyVelocities[i] = new RigidbodyVelocity(pausingRigidbodies[i]);
                pausingRigidbodies[i].angularVelocity = 0;
                pausingRigidbodies[i].velocity = Vector2.zero;
                pausingRigidbodies[i].gravityScale = 0;

                pausingRigidbodies[i].Sleep();
            }
        }
        public override void Resume()
        {
            if (pausingRigidbodies == null)
            {
                return;
            }
            // Rigidbodyの再開
            for (int i = 0; i < pausingRigidbodies.Length; i++)
            {
                if (pausingRigidbodies[i] != null)
                {
                    pausingRigidbodies[i].WakeUp();
                    pausingRigidbodies[i].velocity = rigidbodyVelocities[i].velocity;
                    pausingRigidbodies[i].angularVelocity = rigidbodyVelocities[i].angularVeloccity;
                    pausingRigidbodies[i].gravityScale = rigidbodyVelocities[i].gravityScale;
                }
            }
        }
    }

    class MonoBehaviourPauser : BasePauser
    {
        MonoBehaviour[] pausingMonoBehaviours;

        public MonoBehaviourPauser(Pausable parent) : base(parent) { }

        public override void Pause()
        {
            // MonoBehaviourの停止
            // 子要素から、有効かつこのインスタンスでないもの、IgnoreGameObjectsに含まれていないMonoBehaviourを抽出
            Predicate<MonoBehaviour> monoBehaviourPredicate =
                obj => obj.enabled &&
                       obj != parent.gameObject &&
                       Array.FindIndex(parent.ignoreGameObjects, gameObject => gameObject == obj.gameObject) < 0;
            pausingMonoBehaviours = Array.FindAll(parent.transform.GetComponentsInChildren<MonoBehaviour>(), monoBehaviourPredicate);
            foreach (var monoBehaviour in pausingMonoBehaviours)
            {
                monoBehaviour.enabled = false;
            }

        }
        public override void Resume()
        {
            if (pausingMonoBehaviours == null)
            {
                return;
            }
            foreach (var monoBehaviour in pausingMonoBehaviours)
            {
                if (monoBehaviour != null)
                {
                    monoBehaviour.enabled = true;
                }
            }
        }
    }

    class AnimatorPauser : BasePauser
    {
        Animator[] pausingAnimators;

        public AnimatorPauser(Pausable parent) : base(parent) { }

        public override void Pause()
        {
            // Animatorの停止
            // 子要素から、有効かつこのインスタンスでないもの、IgnoreGameObjectsに含まれていないMonoBehaviourを抽出
            Predicate<Animator> animatorPredicate =
                obj => obj.enabled &&
                       Array.FindIndex(parent.ignoreGameObjects, gameObject => gameObject == obj.gameObject) < 0;
            pausingAnimators = Array.FindAll(parent.transform.GetComponentsInChildren<Animator>(), animatorPredicate);
            foreach (var animator in pausingAnimators)
            {
                animator.enabled = false;
            }
        }
        public override void Resume()
        {
            if (pausingAnimators == null)
            {
                return;
            }
            foreach (var animator in pausingAnimators)
            {
                if (animator != null)
                {
                    animator.enabled = true;
                }
            }
        }
    }

    #endregion
}
