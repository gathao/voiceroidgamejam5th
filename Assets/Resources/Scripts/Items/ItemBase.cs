﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityConstants;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;
using System;

public class ItemBase : MonoBehaviour
{
    [SerializeField]
    Collider2D itemCollider = null;

    [SerializeField]
    protected SpriteRenderer sprite = null;

    [SerializeField]
    Rigidbody2D rigidBody2d = null;

    [SerializeField]
    protected GameObject itemGetWindow = null;

    [SerializeField]
    protected Image itemWindowBack = null;

    [SerializeField]
    protected Text itemNameText = null;

    [SerializeField]
    protected Text itemOverviewText = null;

    bool got = false;

    Coroutine coroutine = null;

    List<IDisposable> disposables = new List<IDisposable>();

    protected virtual void Start()
    {
        // 触れると取得
        itemCollider
            .OnTriggerEnter2DAsObservable()
            .Where(col => col.gameObject.layer == Layers.PlayerKuraiHitbox && !got)
            .Take(1)
            .Subscribe(_ =>
            {
                GetItem();
            }).AddTo(disposables);
    }

    protected virtual void GetItem()
    {
        // momoya
        AudioManager.PlaySe(GameSE.ItemGet);

        var player = PlayerCharacterManager.Instance.Player;

        // ポーズ
        player.isPlayerble = false;
        itemGetWindow.gameObject.SetActive(true);

        Sequence sequence = DOTween.Sequence()
            .OnStart(() =>
            {
                itemWindowBack.transform.SetLocalScaleY(0.0f);
            })
            .Append(itemWindowBack.transform.DOScaleY(1.0f, 0.3f))
            .AppendCallback(() => Time.timeScale = 0)
            .AppendCallback(() =>
            {
                Observable.EveryUpdate()
                .Where(_ => GameInput.Instance.GetButtonDown(GameKey.Submit))
                .Subscribe(_ => 
                {
                    itemGetWindow.gameObject.SetActive(false);

                    // ポーズ解除
                    Time.timeScale = 1;
                    player.isPlayerble = true;
                });
            })
            .Play();
    }

    IEnumerator WaitCoroutine(float sec)
    {
        yield return new WaitForSeconds(sec);

        coroutine = null;

        yield return null;
    }

    private void OnDestroy()
    {
        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }
}
