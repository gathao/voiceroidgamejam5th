﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceJumpItem : ItemBase
{
    protected override void GetItem()
    {
        base.GetItem();

        itemNameText.text = "ジャンプ回数制限解除";
        itemOverviewText.text = "空中ジャンプが無限にできる";

        //AudioManager.PlaySe(GameSE.Heel);

        PlayerCharacterManager.Instance.Player.CanSpaceJump = true;

        Destroy(gameObject);
    }
}
