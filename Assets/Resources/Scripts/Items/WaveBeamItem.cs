﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveBeamItem : ItemBase
{
    protected override void GetItem()
    {
        base.GetItem();

        itemNameText.text = "グレートエレキファイヤー";
        itemOverviewText.text = "敵・壁全て貫通する炎";

        //AudioManager.PlaySe(GameSE.Heel);

        // 装備変更
        PlayerCharacterManager.Instance.Player.BeamWeaponStateMachine.Change(PlayerBeamWeaponState.WaveBeam);

        Destroy(gameObject);
    }
}