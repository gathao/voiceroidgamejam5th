﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaBeamItem : ItemBase
{
    protected override void GetItem()
    {
        base.GetItem();

        itemNameText.text = "グレートエレキビーム";
        itemOverviewText.text = "敵を貫通するビーム しかも3本！";

        //AudioManager.PlaySe(GameSE.Heel);

        // 装備変更
        PlayerCharacterManager.Instance.Player.BeamWeaponStateMachine.Change(PlayerBeamWeaponState.PlasmaBeam);

        Destroy(gameObject);
    }
}
