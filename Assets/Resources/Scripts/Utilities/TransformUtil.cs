﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformUtil
{
    public static Vector2 AngleToDirection(float angle)
    {
        return new Vector2
            (
                Mathf.Cos(angle * Mathf.Deg2Rad),
                Mathf.Sin(angle * Mathf.Deg2Rad)
            );
    }

    public static float DirectionToAngle(Vector2 direction)
    {
        float radian = Mathf.Atan2(direction.y, direction.x);

        if (radian > 0.0f)
        {
            radian = radian + Mathf.PI * 2.0f;
        }

        return radian * Mathf.Rad2Deg;
    }
}
