using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityConstants;

#if UNITY_EDITOR
using UnityEditor;
#endif

public enum AreaCameraType
{
    HorizontalMove,
    VerticalMove,
    BothMove,
}

[Serializable]
public class AreaChangeTrigger
{
    public Map map;
    public BoxCollider2D collider;
}

public class Map : MonoBehaviour
{
    [SerializeField]
    AreaCameraType areaCameraType = AreaCameraType.HorizontalMove;

    [SerializeField]
    Transform leftEdge = null;

    [SerializeField]
    Transform rightEdge = null;

    [SerializeField]
    Transform topEdge = null;

    [SerializeField]
    Transform bottomEdge = null;

    [SerializeField]
    AreaChangeTrigger[] areaChangeTriggers = null;

    [SerializeField]
    Sprite sprite = null;

    public StageBackgroundParam backgroundParam = new StageBackgroundParam()
    {
        Xdistance = 200.0f,
        Ydistance = 220.0f,
        Xoffset = 0.0f,
        Yoffset = 0.16f,
        Xscale = 5.0f,
    };

    public void SetupCamera()
    {
        var camera = FindObjectOfType<GameCameraController>();

        camera.MinX = leftEdge.position.x;
        camera.MaxX = rightEdge.position.x;
        camera.MinY = bottomEdge.position.y;
        camera.MaxY = topEdge.position.y;
    }

    void UpdateBackgroundSprite()
    {
        if (backgroundParam == null)
        {
            return;
        }

        var backgroundManager = FindObjectOfType<BackGroundManager>();
        if (backgroundManager == null)
        {
            return;
        }

        Action<BackgroundImageDistance, StageBackgroundParam, AreaCameraType> changeSprite
            = (dist, param, type) =>
            {
                backgroundManager.ChangeSprite(dist, param);
                backgroundManager.ChangeScrollType(dist, type);
            };

        changeSprite(BackgroundImageDistance.Far, backgroundParam, areaCameraType);

    }

    public void EnterMap()
    {
        SetupCamera();
        UpdateBackgroundSprite();
    }


#if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        var rect = new Rect();

        rect.xMin = leftEdge.transform.position.x;
        rect.xMax = rightEdge.transform.position.x;
        rect.yMin = bottomEdge.transform.position.y;
        rect.yMax = topEdge.transform.position.y;

        Gizmos.color = Color.red;

        var z = transform.position.z;
        var leftBottom = new Vector3(rect.xMin, rect.yMin, z);
        var rightBottom = new Vector3(rect.xMax, rect.yMin, z);
        var rightTop = new Vector3(rect.xMax, rect.yMax, z);
        var leftTop = new Vector3(rect.xMin, rect.yMax, z);

        Gizmos.DrawLine(leftBottom, rightBottom);
        Gizmos.DrawLine(rightBottom, rightTop);
        Gizmos.DrawLine(rightTop, leftTop);
        Gizmos.DrawLine(leftTop, leftBottom);
    }

#endif

    // Start is called before the first frame update
    void Start()
    {
        var areaManager = FindObjectOfType<MapManager>();
        foreach (var areaChangeTrigger in areaChangeTriggers)
        {
            areaChangeTrigger.collider
                .OnTriggerEnter2DAsObservable()
                .Where(col => areaManager.CurrentMap != areaChangeTrigger.map)
                //.Where(col => col.gameObject.layer == Layers.MapChange)
                .Subscribe(col =>   
                {
                    areaManager.ChangeArea(areaChangeTrigger.map);
                }).AddTo(this);
        }
    }
}
