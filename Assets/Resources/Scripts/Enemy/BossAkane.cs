﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UniRx.Triggers;
using UnityConstants;
using DG.Tweening;
using System;

public enum BossAkaneMoveState
{
    Wait,
    Startup,
    Battle01,
    Battle02,
    Dead,
}

public class BossAkane : EnemyBase
{
    [SerializeField]
    EnemyBeam001 beam001 = null;

    [SerializeField]
    Transform akaneHontaiTranform = null;

    [SerializeField]
    BoxCollider2D bossRoomDoor = null;

    Vector3 shotOffset = new Vector2(-0.3f, 0.0f);

    Coroutine attakCoroutine001 = null;

    BossAkaneMoveState currentMoveState = BossAkaneMoveState.Wait;

    [SerializeField]
    BoxCollider2D StartButtleCollider　= null;

    [SerializeField]
    GameObject BossAkaneCutin = null;

    [SerializeField]
    GameObject YukariCutin = null;


    void Start()
    {
        Setup();
    }

    public override void Setup()
    {
        base.Setup();

        enemyType = EnemyType.Boss_Akane;
        EnemyName = "ボスあかね";

        SetBodyAttack(10);

        currentMoveState = BossAkaneMoveState.Wait;

        this.EnemyKuraiHitBox.gameObject.SetActive(false);

        var player = PlayerCharacterManager.Instance.Player;

        StartButtleCollider
            .OnTriggerEnter2DAsObservable()
            .Where(_ => currentMoveState == BossAkaneMoveState.Wait)
            .Where(obj => obj.gameObject.layer == Layers.PlayerKuraiHitbox)
            .Subscribe(_ =>
            {
                currentMoveState = BossAkaneMoveState.Startup;

                bossRoomDoor.gameObject.SetActive(true);

                // ステージ上の雑魚的を削除する
                var enemies = GameObject.FindObjectsOfType<AkarikusaEnemy>();
                foreach (var enemy in enemies)
                {
                    enemy.Dispose();
                    Destroy(enemy.gameObject);
                }

                // プレイヤーの操作を受け付けなくする
                player.isPlayerble = false;

                // カットイン演出
                Sequence sequence = DOTween.Sequence()
                    .OnStart(() =>
                    {
                        BossAkaneCutin.transform.localPosition = new Vector2(1100.0f, 0.0f);
                        YukariCutin.transform.localPosition = new Vector2(-1100.0f, 0.0f);
                    })
                    .Append(BossAkaneCutin.transform.DOLocalMoveX(0.0f, 0.5f))
                    .Join(YukariCutin.transform.DOLocalMoveX(0.0f, 0.5f))
                    .AppendInterval(1.0f)
                    .Append(BossAkaneCutin.transform.DOLocalMoveX(-1100.0f, 0.5f))
                    .Join(YukariCutin.transform.DOLocalMoveX(1100.0f, 0.5f))
                    .AppendCallback(() =>
                    {
                        // 操作を受け付けるようにする
                        player.isPlayerble = true;

                        // ボスの無敵解除
                        this.EnemyKuraiHitBox.gameObject.SetActive(true);

                        // 戦闘開始
                        currentMoveState = BossAkaneMoveState.Battle01;

                        AudioManager.PlayBgm(GameBGM.Boss);
                    })
                    .Play();
            })
            .AddTo(enemyActions);

        this.FixedUpdateAsObservable()
            //.Where(_ => currentMoveState == BossAkaneMoveState.Battle01)
            .Subscribe(_ =>
            {
                LookPlayer();

                if (currentMoveState == BossAkaneMoveState.Battle01)
                {
                    // バトル開始
                    if (attakCoroutine001 == null)
                    {
                        attakCoroutine001 = StartCoroutine(Attack01Coroutine());
                    }

                    // HPが150以下になると行動変化
                    if (Hp.Value <= 150.0f)
                    {
                        Debug.Log("第一形態おわり");

                        StopCoroutine(Attack01Coroutine());
                        attakCoroutine001 = null;

                        currentMoveState = BossAkaneMoveState.Battle02;
                    }
                }
                else if(currentMoveState == BossAkaneMoveState.Battle02)
                {

                }

            })
            .AddTo(enemyActions);

        Deaded
            .Where(v => v)
            .Subscribe(_ =>
            {
                Debug.Log("やられたー");

                AudioManager.StopBgm();

                DOTween.Sequence()
                    .AppendInterval(0.3f)
                    .AppendCallback(() =>
                    {
                        AudioManager.PlaySe(GameSE.Explosion);
                    })
                    .AppendInterval(0.3f)
                    .AppendCallback(() =>
                    {
                        AudioManager.PlaySe(GameSE.Explosion);
                    })
                    .Play();

                // 無敵フラグを立てる
                player.SetInvinsible(int.MaxValue);

                // プレイヤーの操作を受け付けなくする
                player.isPlayerble = false;


                Sequence sequence = DOTween.Sequence()
                    .AppendInterval(2.0f)
                    .AppendCallback(() =>
                    {
                        AudioManager.PlaySe(GameSE.Fanfale);

                        player.LookRight();
                        player.SetAnimation(PlayerAnimationState.Walk);
                    })
                    .AppendInterval(1.0f)
                    .AppendCallback(() =>
                    {
                        player.LookLeft();
                        player.SetAnimation(PlayerAnimationState.Walk);
                    })
                    .AppendInterval(1.0f)
                    .AppendCallback(() =>
                    {
                        player.LookRight();
                        player.StopWalkAnim();
                        player.SetAnimation(PlayerAnimationState.KnockBack);
                    })
                    .AppendInterval(0.5f)
                    .AppendCallback(() =>
                    {
                        player.LookLeft();
                        player.StopWalkAnim();
                        player.SetAnimation(PlayerAnimationState.KnockBack);
                    })
                    .AppendInterval(0.5f)
                    .AppendCallback(() =>
                    {
                        player.LookLeft();
                        player.StopWalkAnim();
                        player.SetAnimation(PlayerAnimationState.Finish);
                    })
                    .AppendInterval(3.0f)
                    .AppendCallback(() =>
                    {
                        // ファンファーレ終了　感動のタイトル画面へ…
                        AppManager.Instance.ChangeToTitleScene();
                    })
                    .Play();
            })
            .AddTo(disposables);
    }

    void ShotEnemyBeam001(float shotAngle, float speed)
    {
        var normalBeam = GameObject.Instantiate(beam001).GetComponent<EnemyBeam001>();
        normalBeam.SetUp(akaneHontaiTranform.position + shotOffset, shotAngle, speed, WeaponOwner.Enemy);
        normalBeam.SetDamage(8.0f);
        normalBeam.WeaponStart();
    }

    // 自機狙い弾を打つ
    IEnumerator Attack01Coroutine()
    {
        while(true)
        {
            for (int i=0; i < 8; i++)
            {
                Vector2 direction = PlayerCharacterManager.Instance.Player.CentorPos - (akaneHontaiTranform.position + shotOffset);
                float shotAngle = TransformUtil.DirectionToAngle(direction);

                ShotEnemyBeam001(shotAngle, 8.0f);
                AudioManager.PlaySe(GameSE.EnemyShot);

                yield return new WaitForSeconds(0.45f);
            }

            yield return new WaitForSeconds(1.3f);


        }
    }

    // ランダム弾を撃つ
    IEnumerator Attack02Coroutine()
    {
        while (true)
        {
            // 弾発射
            //Vector2 direction = PlayerCharacterManager.Instance.Player.CentorPos - (transform.position + shotOffset);
            //this.shotAngle = TransformUtil.DirectionToAngle(direction);

            yield return new WaitForSeconds(0.5f);
        }
    }

}
