﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniRx.Triggers;
using UniRx;
using UnityConstants;
using System;


public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    EnemyType enemyType = EnemyType.AkariKusa;

    [SerializeField]
    CircleCollider2D checkCameraViewingCollider = null;

    bool isSpawnable = true;

    List<IDisposable> disposables = new List<IDisposable>();

    EnemyBase spawnedEnemy;

    public void Start()
    {
        Setup();
    }

    public void Setup()
    {
        // 初期状態でカメラ内にいたらスポーン実行
        if (checkCameraViewingCollider.IsTouchingLayers(Layers.CameraBoundingBox))
        {
            Spawn();
        }

        // カメラの範囲内に入ったらスポーン実行
        checkCameraViewingCollider
            .OnTriggerEnter2DAsObservable()
            .Where(col => col.gameObject.layer == Layers.CameraBoundingBox)
            .Subscribe(_ =>
            {
                Spawn();
            });
    }

    public void Spawn()
    {
        if (spawnedEnemy != null)
        {
            return;
        }

        Debug.Log("スポーン!");

        // Enemyのインスタンス生成
        spawnedEnemy = InstantiateEnemy(EnemyMasterData.Instance[enemyType].Prefab);
    }

    private EnemyBase InstantiateEnemy(GameObject enemyPrefab)
    {
        var enemy = Instantiate(enemyPrefab).GetComponent<EnemyBase>();
        enemy.transform.SetParent(transform);
        enemy.transform.position = transform.position;
        enemy.Setup();
        //enemy.Setup(GetComponent<EnemyInstantiateData>());

        return enemy;
    }

    void Dispose()
    {
        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }
}
