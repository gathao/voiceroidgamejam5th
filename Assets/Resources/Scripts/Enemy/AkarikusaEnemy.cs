﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UniRx.Triggers;
using UnityConstants;
using DG.Tweening;
using System;

public class AkarikusaEnemy : EnemyBase
{
    [SerializeField]
    EnemyBeam001 beamPrefab = null;

    Vector3 shotOffset = new Vector2(0.25f, 0.15f);

    Coroutine attakCoroutine = null;

    float shotAngle = 0.0f;

    void Start()
    {
        Setup();
    }

    public override void Setup()
    {
        base.Setup();

        enemyType = EnemyType.AkariKusa;
        EnemyName = "あかり草";

        SetBodyAttack(5);

        this.FixedUpdateAsObservable()
            .Subscribe(_ =>
            {
                LookPlayer();

                // プレイヤーが近づくと攻撃する
                if (IsPlayerNeighbor(8.5f))
                {
                    if (attakCoroutine == null)
                    {
                        attakCoroutine = StartCoroutine(AttackCoroutine());
                    }
                }
                else
                {
                    if (attakCoroutine != null)
                    {
                        StopCoroutine(attakCoroutine);
                        attakCoroutine = null;
                    }
                    
                }

            })
            .AddTo(enemyActions);
    }

    IEnumerator AttackCoroutine()
    {
        while(true)
        {
            Vector2 direction = PlayerCharacterManager.Instance.Player.CentorPos - (transform.position + shotOffset);
            this.shotAngle = TransformUtil.DirectionToAngle(direction);

            ShotEnemyBeam();
            yield return new WaitForSeconds(0.2f);

            ShotEnemyBeam();
            yield return new WaitForSeconds(0.2f);

            ShotEnemyBeam();
            yield return new WaitForSeconds(3.0f);
        }
    }

    void ShotEnemyBeam()
    {
        var normalBeam = GameObject.Instantiate(beamPrefab).GetComponent<EnemyBeam001>();
        normalBeam.SetUp(transform.position + shotOffset, this.shotAngle, 5.0f, WeaponOwner.Enemy);
        normalBeam.SetDamage(6.0f);
        normalBeam.WeaponStart();

        AudioManager.PlaySe(GameSE.EnemyShot);
    }
}
