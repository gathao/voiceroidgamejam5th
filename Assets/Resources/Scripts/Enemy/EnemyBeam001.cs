﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniRx.Triggers;
using UniRx;
using UnityConstants;
using System;


public class EnemyBeam001 : BeamBase
{
    [SerializeField]
    SpriteRenderer beamSprite1 = null;

    [SerializeField]
    SpriteRenderer beamSprite2 = null;

    Coroutine coroutine = null;

    public override void SetUp(Vector3 position, float angle, float speed, WeaponOwner weaponOwner)
    {
        base.SetUp(position, angle, speed, weaponOwner);
    }

    public override void WeaponStart()
    {
        beamSprite1.gameObject.SetActive(true);
        beamSprite2.gameObject.SetActive(false);

        attackCollider.gameObject.SetActive(true);

        // 弾が飛んでいくベクトル
        rigidbody2D.velocity = Quaternion.Euler(0, 0, angle) * new Vector2(speed, 0.0f);

        StartCoroutine(AnimationCoroutine());

        // 壁に当たった時の処理
        //attackCollider
        //    .OnTriggerEnter2DAsObservable()
        //    .Where(col => col.gameObject.layer == Layers.Ground)
        //    .Subscribe(_ =>
        //    {
        //        WeaponHit();
        //    })
        //    .AddTo(disposables);
    }

    IEnumerator AnimationCoroutine()
    {
        while (true)
        {
            beamSprite1.gameObject.SetActive(true);
            beamSprite2.gameObject.SetActive(false);

            yield return new WaitForSeconds(0.1f);

            beamSprite1.gameObject.SetActive(false);
            beamSprite2.gameObject.SetActive(true);

            yield return new WaitForSeconds(0.1f);
        }
    }

    public override void WeaponHit()
    {
        rigidbody2D.velocity = Vector2.zero;

        attackCollider.gameObject.SetActive(false);

        this.FixedUpdateAsObservable()
            .Where(_ => coroutine == null)
            .Subscribe(_ =>
            {
                StopCoroutine(AnimationCoroutine());
                Dispose();
                Destroy(gameObject);
            })
            .AddTo(disposables);
    }

}
