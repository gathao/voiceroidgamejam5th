﻿using System.Linq;
using UnityEngine;
using UnityConstants;
using UniRx;
using UniRx.Triggers;
using System.Collections;
using DG.Tweening;
using System;
using System.Collections.Generic;

public class EnemyBase : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D Rigidbody2D = null;

    [SerializeField]
    protected BoxCollider2D EnemyKuraiHitBox = null;

    [SerializeField]
    protected BoxCollider2D BodyAttackCollider = null;

    public EnemyType enemyType { get; protected set; }

    protected List<IDisposable> disposables = new List<IDisposable>();

    protected List<IDisposable> enemyActions = new List<IDisposable>();

    public FloatReactiveProperty Hp { get; protected set; }

    [SerializeField]
    float HitPoint = 2.0f;

    public ReadOnlyReactiveProperty<bool> Deaded { get; private set; }

    public readonly BoolReactiveProperty FacingRight = new BoolReactiveProperty(true);

    // 胴体にヒットした時のダメージ
    public int BodyAttackDamage { get; set; }

    public string EnemyName { get; set; }

    // 無敵フラグ
    public readonly BoolReactiveProperty IsInvinsible = new BoolReactiveProperty(false);

    bool isSetuped = false;

    // Start is called before the first frame update
    void Start()
    {
        Hp = new FloatReactiveProperty(HitPoint);

        Setup();
    }

    public virtual void Setup()
    {
        if (isSetuped)
        {
            return;
        }

        Hp = new FloatReactiveProperty(HitPoint);

        Deaded = Hp
            .Select(hp => hp <= 0)
            .ToReadOnlyReactiveProperty();

        Deaded
            .Where(v => v)
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.Explosion);

                //Debug.Log(EnemyName + "死亡");
                //Dispose();
                PlayDeadAnimation();
            })
            .AddTo(disposables);

        isSetuped = true;
    }

    // 接触したときの攻撃判定
    public void SetBodyAttack(int damage = 1)
    {
        this.BodyAttackDamage = damage;

        this.BodyAttackCollider
            .OnTriggerStay2DAsObservable()
            .Where(col => col.gameObject.layer == Layers.PlayerKuraiHitbox)
            .Where(_ => !PlayerCharacterManager.Instance.Player.IsInvinsible.Value)
            .Subscribe(_ =>
            {
                PlayerCharacterManager.Instance.Player.DamageByEnemyAttack(this.BodyAttackDamage);
            })
            .AddTo(enemyActions);
    }

    public virtual void Damage(float damage)
    {
        if (Hp.Value <= damage)
        {
            Hp.Value = 0;
        }
        else
        {
            Hp.Value -= damage;
            AudioManager.PlaySe(GameSE.EnemyDead);
        }
    }

    protected void PlayDeadAnimation()
    {
        //EffectManager.Instance.PlayOnce(EffectType.EnemyBurst, transform.position);

        Dispose();
        Destroy(gameObject);
    }

    public void SetInvicible(bool value)
    {
        IsInvinsible.Value = value;
    }

    // 右を向く
    public void LookRight()
    {
        if (FacingRight.Value)
        {
            return;
        }
        FacingRight.Value = true;
        var tmp = transform.localScale;
        tmp.x = Mathf.Abs(tmp.x);
        transform.localScale = tmp;
    }

    // 左を向く
    public void LookLeft()
    {
        if (!FacingRight.Value)
        {
            return;
        }
        FacingRight.Value = false;
        var tmp = transform.localScale;
        tmp.x = -Mathf.Abs(tmp.x);
        transform.localScale = tmp;
    }

    // 現在と逆を向く
    public void FacingFlip()
    {
        if (FacingRight.Value)
        {
            LookLeft();
        }
        else
        {
            LookRight();
        }
    }

    // プレイヤーのいる方向を向く
    public void LookPlayer()
    {
        var playerX = PlayerCharacterManager.Instance.Player.transform.position.x;
        if (transform.position.x < playerX)
        {
            LookRight();
        }
        else
        {
            LookLeft();
        }
    }

    /// プレイヤーが円内に存在しているか判定
    protected bool IsPlayerNeighbor(float radius)
    {
        var playerPos = PlayerCharacterManager.Instance.Player.transform.position + new Vector3(0, 1f);
        var pos = transform.position;

        return (playerPos.x - pos.x) * (playerPos.x - pos.x) + (playerPos.y - pos.y) * (playerPos.y - pos.y) <= radius * radius;
    }

    public void AddDisposable(IDisposable obj)
    {
        disposables.Add(obj);
    }

    public virtual void ActionsDispose()
    {
        foreach (var item in enemyActions)
        {
            item.Dispose();
        }
        enemyActions.Clear();
    }

    public virtual void Dispose()
    {
        transform.DOKill(false);
        foreach (var item in disposables)
        {
            item.Dispose();
        }
        disposables.Clear();
    }
}
