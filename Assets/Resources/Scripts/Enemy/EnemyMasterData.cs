﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    AkariKusa,
    Boss_Akane,
}

public class EnemyMasterDataItem
{
    public EnemyType EnemyType;
    public GameObject Prefab;
}

public class EnemyMasterData : SingletonMonoBehaviour<EnemyMasterData>
{
    [SerializeField]
    GameObject AkarikusaEnemyPrefab = null;

    [SerializeField]
    GameObject BossAkanePrefab = null;

    // 敵キャラのリストデータ
    [SerializeField]
    List<EnemyMasterDataItem> data = new List<EnemyMasterDataItem>();

    public EnemyMasterDataItem this[EnemyType type]
    {
        get
        {
            return data.Find(val => val.EnemyType == type);
        }
    }

    public void Awake()
    {
        data.Add(new EnemyMasterDataItem()
        {
            EnemyType = EnemyType.AkariKusa,
            Prefab = AkarikusaEnemyPrefab,
        });

        data.Add(new EnemyMasterDataItem()
        {
            EnemyType = EnemyType.Boss_Akane,
            Prefab = BossAkanePrefab,
        });
    }
}
