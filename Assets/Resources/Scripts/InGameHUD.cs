﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
using DG.Tweening;

public class InGameHUD : MonoBehaviour
{
    [SerializeField]
    Text HPNameText = null;

    [SerializeField]
    Text HPText = null;

    readonly Color NormalHPColor = new Color(0.9137f, 0.6431f, 0.1098f);

    readonly Color AlertHPColor = new Color(0.9137f, 0.2862f, 0.1098f);


    void Start()
    {
        var playerManager = PlayerCharacterManager.Instance;
        var player = PlayerCharacterManager.Instance.Player;
        var maxHP = PlayerCharacterManager.MaxHP;

        playerManager.Hp.Subscribe(hp =>
        {
            HPText.text = ((int)hp).ToString();

            // HPが少なくなると赤くなる
            if (hp <= 30.0f)
            {
                HPNameText.color = AlertHPColor;
                HPText.color = AlertHPColor;
            }
            else
            {
                HPNameText.color = NormalHPColor;
                HPText.color = NormalHPColor;
            }
        });


    }
}
